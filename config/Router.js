import React, {
    Component
} from 'react';
import {
    Root,
    Image
} from "native-base";
import {
    createStackNavigator
} from 'react-navigation'; // Version can be specified in package.json
import SplaschScreen from '../splash_screen/SplashScreen';
import Login from '../login/Login';
import ListHandover from '../list_handover/ListHandover';
import CreateHandover from '../list_handover/CreateHandover';
import ViewHandover from '../list_handover/ViewHandover';
import EditHandover from '../list_handover/EditHandover';

const RouterNav = createStackNavigator({
        SplaschScreen: {
            screen: SplaschScreen
        },
        LoginScreen: {
            screen: Login
        },
        ListHandoverScreen: {
            screen: ListHandover,
        },
        CreateHandoverScreen: {
            screen: CreateHandover,
        },
        ViewHandoverScreen: {
            screen: ViewHandover,
        },
        EditHandoverScreen: {
            screen: EditHandover,
        },
        // DetailHistory: {
        // 	screen: DetailHistory,
        // 	navigationOptions: ({ navigation }) => ( NOption(navigation , "Detail History") )
        // },
    }, {
        mode: 'modal',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#CB2406',
            },
            headerTintColor: '#fff',
            headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', flex:1, textAlign: 'center' },
        },

    },

);
export default RouterNav;