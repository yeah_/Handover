// const BASE_URL = "http://ob.lippohomes.com:8457";
// const BASE_URL = "http://engine3uat.vnetcloud.com:8091";
const BASE_URL= "http://engine2dev.vnetcloud.com:8030";

const CONST = {
    BASE_URL : BASE_URL,
    YEAR : '&year=',
    CONFIRM_DATE_YEAR : '&confirmDateYear=',
    PERIODE_ID : '&PeriodID=',
    CONFIRM_DATE : '&ConfimDate=',
    UNIT_CODE : '&unitcode=',
    PAYMENT_STATUS : '&StatusPayment=',
    TEXT_SEARCH : '&textSearch=',
    INVITATION_STATUS : '&InvitationStatus=',
    LOGIN : BASE_URL + '/api/TokenAuth/Authenticate',
    GET_ALL_INVITATION : BASE_URL + '/api/services/app/HandoverMobile/GetAllInvitation?confirmDateMonth=',
    CREATE_HANDOVER : BASE_URL + '/api/services/app/HandoverMobile/CreateHandover',
    GET_HANDOVER_DETAIL : BASE_URL + '/api/services/app/HandoverMobile/GetHandoverDetail?invitationID=',
    GET_PERIODE_LIST : BASE_URL + '/api/services/app/HandoverMobile/GetPeriodList',
    GET_CONFIRM_DATE_LIST : BASE_URL + '/api/services/app/HandoverMobile/GetConfirmDateList?month=',
    GET_UNIT_CODE_LIST : BASE_URL + '/api/services/app/HandoverMobile/GetUnitCodeList?month=',
    UPDATE_HANDOVER : BASE_URL + '/api/services/app/HandoverMobile/UpdateHandover',
    GET_PAYMENT_STATUS : BASE_URL +'/api/services/app/HandoverMobile/GetStatusPayemntList',
    GET_ROOM_LIST : BASE_URL +'/api/services/app/HandoverMobile/GetRoomList',
    GET_STATUS_INVITATION : BASE_URL +'/api/services/app/HandoverMobile/GetStatusInvitationList',
    GET_DEFECT_LIST : BASE_URL +'/api/services/app/HandoverMobile/GetDefectList?roomid=',
    UNIVERSAL_CREATE_HANDOVER : BASE_URL + '/api/services/app/HandoverMobile/UniversalCreateHandover',
    GET_PARAM_API_LK : BASE_URL + '/api/services/app/HandoverMobile/GetParamAPILK'
};

export default CONST