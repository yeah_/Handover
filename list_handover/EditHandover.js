import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    Animated,
    View,
    Image,
    CheckBox,
    TextInput,
    Button,
    BackHandler,
    TouchableHighlight,
    AsyncStorage,
    ActivityIndicator,
    Alert
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import { HeaderBackButton } from 'react-navigation';
import { Dropdown } from 'react-native-material-dropdown';
import UrlApi from '../config/UrlApi';
import Modal from 'react-native-modalbox';
import axios from 'axios';
import _ from 'lodash';
import dateFormat from 'dateformat';

export default class EditHandover extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isCheckedBast: false,
            isCheckedSerah: false,
            isCheckedSmartKey: false,
            isCheckedHandover: false,
            isCheckedTata: false,
            isCheckedPinjam: false,
            isCheckedBonus: false,
            valueArray: [],
            data: [],
            detail: [],
            disabled: false,
            bonusDescription: '',
            bastNo: '',
            isLoading: true,
            isLoadingDefect: false,
            isLoadingButton: false,
            isValidDefect: true,
            isModalImageDefect: false,
            isModalImageBAST: false,
            isModalImagePinjam: false,
            imagePopupDefect: '',
            imagePopupBAST: '',
            imagePopupPinjam: '',
            resizedImageUri: '',
            exRoom: [],
            bastDate: '',
            token: '',
            imageBASTArray: [],
            imagePinjamArray: [],
            hasilDefect: [],
            defectList: [],
            removeImagePopupBASTindex: 0,
            removeImagePopupPinjamindex: 0,
            defectID: [],
            detailDefectList: [],

            arrayAPILK:[],
            siteID: '',
            orgID: '',
            unitCode: '',
            unitNo: '',
            coCode: '',
            bookCode: '',
            psCode: '',
            name: '',
            bastType: '',
            stType: '',
            remarks: '',
            username: '',
            requestorName: '',
            requestorPhone: '',
            requestorEmail: '',
            type: '',
            fileType: '',
            handoverDetails: [],
            caseNumber:'',
            bastID:'',

            
        }
        this.today = new Date();
        this.params = this.props.navigation.state.params;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.index = 0;
        this.animatedValue = new Animated.Value(0);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: "Edit Hand Over",
            headerLeft: (
                <HeaderBackButton
                    color="#fff"
                    tintColor="#fff"
                    onPress={() => {
                        Alert.alert(
                            'Exit App',
                            'Are you sure you want to go back?', [
                            {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel'
                            }, {
                                text: 'OK',
                                onPress: () => navigation.navigate('ListHandoverScreen')
                            },], {
                            cancelable: false
                        }
                        )
                    }} />
            ),
        }
    }

    alertMessage(message) {
        Alert.alert(
            'Warning',
            'Please checked the ' + message + ' checklist first!',
        )
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillMount() {
        this.getToken()
        this.props.navigation.addListener('didFocus', () => {
            this.loadDetailHandover();
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async getToken() {
        try {
            let token = await AsyncStorage.getItem('accessToken');
            return this.setState({
                token: token
            })
        }
        catch (e) {
            console.log('caught error', e);
            // Handle exceptions
        }

    }

    handleBackButtonClick() {
        Alert.alert(
            'Exit App',
            'Are you sure you want to go back?', [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('ListHandoverScreen')
            },], {
            cancelable: false
        }
        )
        return true;
    }

    onChangeBonus(bonusDescription) {
        this.setState({
            bonusDescription: bonusDescription
        })
    }

    checkBoxBastOnChage() {
        this.setState({
            isCheckedBast: !this.state.isCheckedBast
        })
    }

    checkBoxSerahOnChage() {
        this.setState({
            isCheckedSerah: !this.state.isCheckedSerah
        })
    }

    checkBoxSmartKeyOnChage() {
        this.setState({
            isCheckedSmartKey: !this.state.isCheckedSmartKey
        })
    }

    checkBoxHandoverOnChage() {
        this.setState({
            isCheckedHandover: !this.state.isCheckedHandover
        })
    }

    checkBoxTataOnChage() {
        this.setState({
            isCheckedTata: !this.state.isCheckedTata
        })
    }

    checkBoxPinjamOnChage() {
        this.setState({
            isCheckedPinjam: !this.state.isCheckedPinjam,
        })
    }

    checkBoxBonusOnChage() {
        this.setState({
            isCheckedBonus: !this.state.isCheckedBonus
        })
    }

    takePhotoBAST(cropping, mediaType = 'photo') {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 200,
            height: 200,
            // compressImageMaxWidth: 200,
            // compressImageMaxHeight: 200,
            compressImageQuality: 1,
            includeExif: true,
            mediaType,
        }).then(imageBAST => {
            console.log('received image', imageBAST);
            this.setState({
                imageBAST: { uri: imageBAST.path, width: imageBAST.width, height: imageBAST.height, mime: imageBAST.mime },
                imagesBAST: null
            });

            let d = this.today.getDate();
            let m = this.today.getMonth('MM') + 1;
            let y = this.today.getFullYear();
            let mS = m.toString();
            let dS = d.toString();
            let lengthmS = mS.length;
            let lengthdS = dS.length;
            let monthValue = '';
            let dayValue = '';

            if (lengthmS == 1) {
                monthValue = '0' + mS
            } else {
                monthValue = mS
            }

            if (lengthdS == 1) {
                dayValue = '0' + dS
            } else {
                dayValue = dS
            }

            let dateString = y + '-' + monthValue + '-' + dayValue
            if (this.state.bastDate == '' || this.state.bastDate == null) {
                this.setState({
                    bastDate: dateString
                })
            }

            ImgToBase64.getBase64String(this.state.imageBAST.uri)
                .then(base64String => {
                    var imageBASTArray = this.state.imageBASTArray;
                    imageBASTArray.push({
                        imagebase64: base64String
                    })
                    this.setState({
                        imageBASTArray: imageBASTArray
                    })
                    console.log('ParamsImage', imageBASTArray)
                }).catch(err => console.log(err));
        }).catch(e => console.log(e));
    }

    takePhotoPinjam(cropping, mediaType = 'photo') {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 200,
            height: 200,
            // compressImageMaxWidth: 200,
            // compressImageMaxHeight: 200,
            compressImageQuality: 1,
            includeExif: true,
            mediaType,
        }).then(imagePinjam => {
            console.log('received image', imagePinjam);
            this.setState({
                imagePinjam: { uri: imagePinjam.path, width: imagePinjam.width, height: imagePinjam.height, mime: imagePinjam.mime },
                imagePinjam: null
            });
            ImgToBase64.getBase64String(imagePinjam.path)
                .then(base64String => {
                    var imagePinjamArray = this.state.imagePinjamArray;
                    imagePinjamArray.push({
                        imagebase64: base64String
                    })
                    this.setState({
                        imagePinjamArray: imagePinjamArray
                    })
                })
                .catch(err => console.log(err));
        }).catch(e => console.log(e));
    }

    takePhotoDefect(cropping, mediaType = 'photo', parentId, subId) {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 200,
            height: 200,
            // compressImageMaxWidth: 200,
            // compressImageMaxHeight: 200,
            compressImageQuality: 1,
            includeExif: true,
            mediaType,
        }).then(imageDefect => {
            console.log('received image', imageDefect);
            this.setState({
                imageDefect: { uri: imageDefect.path, width: imageDefect.width, height: imageDefect.height, mime: imageDefect.mime },
                imagesDefect: null,
            }
            );
            ImgToBase64.getBase64String(imageDefect.path)
                .then(base64String => {
                    let exRoomNew = [...this.state.exRoom]
                    exRoomNew[parentId].form[subId].imagebase64 = base64String
                    this.setState({
                        exRoom: exRoomNew,
                    })
                })
                .catch(err => console.log(err));
        }).catch(e => console.log(e));
    }

    loadRoom() {
        axios.get(UrlApi.GET_ROOM_LIST, {
            headers: {
                'content-type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            }
        })
            .then((response) => {
                if (response.data.result.status) {
                    this.setState({
                        exRoom: response.data.result.data
                    })
                    const room = this.state.exRoom.map((room) => {
                        return { ...room, form: [], defectList: [] }
                    })
                    this.setState({
                        exRoom: room
                    })
                    // alert(JSON.stringify(this.state.exRoom))
                    var form = [...this.state.exRoom]
                    var formFix = []
                    var defectList = []
                    this.state.detail.map((item) => {
                        this.state.exRoom.map((val, i) => {
                            axios.get(UrlApi.GET_DEFECT_LIST + item.roomID, {
                                headers: {
                                    'content-type': 'application/json',
                                    'Authorization': 'Bearer ' + this.state.token
                                }
                            }).then((response) => {
                                if (response.data.result.status) {
                                    response.data.result.data.map((item) => {
                                        defectList.push({
                                            id: item.id,
                                            name: item.name
                                        })
                                    })
                                    if (item.roomID == val.id) {
                                        form[i].form.push({
                                            roomID: item.roomID,
                                            remarks: item.remarks,
                                            imagebase64: item.imagebase64
                                        })
                                        defectList.map((res) => {
                                            form[i].defectList.push({
                                                id: res.id,
                                                name: res.name
                                            })
                                        })
                                    }
                                    this.setState({
                                        exRoom: form
                                    })

                                    this.state.exRoom.map((item) => {
                                        if (item.form.length != 0 || item.defectList.length != 0) {
                                            formFix.push({
                                                id: item.id,
                                                name: item.name,
                                                form: item.form,
                                                defectList: item.defectList
                                            })
                                        }
                                    })

                                    const values =
                                        _.chain(formFix)
                                            .uniqWith(_.isEqual)
                                            .value()

                                    this.setState({
                                        exRoom: values,
                                    })
                                }
                                // alert(JSON.stringify(this.state.exRoom))
                            }).catch((error) => {
                                console.log(error);
                                // alert(JSON.stringify(error))
                                Alert.alert('Warning', error.response.data.error.message,
                                    [{
                                        text: 'OK',
                                        onPress: () => this.navigate('ListHandoverScreen')
                                    }],
                                    { cancelable: false }
                                );
                            });
                        })
                    })
                }
                this.setState({
                    isLoading: false
                })
            })
            .catch((error) => {
                console.log(error);
                Alert.alert('Warning', error.response.data.error.message,
                    [{
                        text: 'OK',
                        onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                    }],
                    { cancelable: false }
                );
            });
    }

    
    renderRoomDefect() {
        return this.state.exRoom.map((item, key) => {
            return (
                <View>
                    <View style={styles.containerItem}>
                        <View style={{ width: '10%' }}>
                            <Image
                                style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                source={require('../assets/placeholder.png')}
                            />
                        </View>
                        <View style={{ width: '90%' }}>
                            <Text style={{ color: '#CB2406', fontWeight: 'bold' }}>{item.name}</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 5 }}>
                        {item.form ? item.form.map((val, i) => {
                            var imageBase64Defect = 'data:image/png;base64,' + val.imagebase64;

                            return (
                                <View style={styles.cardDefect}>
                                    <TouchableHighlight
                                        onPress={() => this.showImageDefect(imageBase64Defect)}>
                                        {val.imagebase64 == '' ? <View /> :
                                            <Image style={{ width: 40, height: 40, resizeMode: 'contain', marginLeft: 5 }} source={{ uri: imageBase64Defect }} />}
                                    </TouchableHighlight>
                                    <Dropdown
                                        containerStyle={{ marginHorizontal: 10, marginTop: -20, width: val.imagebase64 == '' ? '100%' : '70%' }}
                                        label='Remarks'
                                        data={item.defectList}
                                        value={val.remarks}
                                        fontSize={12}
                                        valueExtractor={({ id }) => id}
                                        labelExtractor={({ name }) => name}
                                        disabled={true}
                                        itemColor='#000'
                                        selectedItemColor='#BB0A0A'
                                    />
                                    {/* <TextInput
                                    editable={false}
                                        style={{borderColor: '#cecece' ,borderWidth: 1, marginLeft: 10, width: val.imagebase64 == '' ? '100%' : '75%'}}
                                        maxLength={500}
                                        value={val.remarks || ''}/> */}
                                </View>
                            );
                        }) : <View />}
                    </View>
                </View>
            )
        })
    }

    addDefect(index, roomID) {
        // this.loadDefectList(roomID)
        this.animatedValue.setValue(0);
        let arrDetail = [...this.state.exRoom];
        let arrRoomId = arrDetail[index].form;
        let arrDefectList = arrDetail[index].defectList;

        arrRoomId.push({
            roomID: roomID,
            imagebase64: '',
            remarks: '-'
        })

        this.setState({ disabled: true, valueArray: arrRoomId }, () => {
            Animated.timing(
                this.animatedValue,
                {
                    toValue: 1,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start(() => {
                this.setState({ disabled: false, isValidDefect: true });
            });
        });
        axios.get(UrlApi.GET_DEFECT_LIST + roomID, {
            headers: {
                'content-type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            }
        })
            .then((response) => {
                if (response.data.result.status) {
                    if (arrDefectList.length != response.data.result.data.length) {
                        arrDefectList.push({
                            id: '-',
                            name: 'Nothing Selected'
                        })
                        response.data.result.data.map((item) => {
                            arrDefectList.push({
                                id: item.id,
                                name: item.name
                            })
                        })
                    }
                }
            })
            .catch((error) => {
                console.log(error);
                // alert(JSON.stringify(error))
                Alert.alert('Warning', error.response.data.error.message,
                    [{
                        text: 'OK',
                        onPress: () => this.navigate('ListHandoverScreen')
                    }],
                    { cancelable: false }
                );
            });

        // alert(JSON.stringify(this.state.exRoom))
        // alert(JSON.stringify(this.state.valueArray))
    }

    removeDefect(parentId, subId) {
        let exRoomNew = [...this.state.exRoom]
        exRoomNew[parentId].form.splice(subId, 1);
        this.setState({
            exRoom: exRoomNew
        })
    }

    removeBAST(index) {
        this.setState((prevState) => ({
            imageBASTArray: prevState.imageBASTArray.filter((_, i) => i !== index)
        }));
        this.setState({
            isModalImageBAST: false
        })
    }

    multipleImageBast() {
        return this.state.imageBASTArray.map((item, key) => {
            var imageBase64Bast = 'data:image/png;base64,' + item.imagebase64;

            return (

                <TouchableHighlight
                    style={styles.noPhoto}
                    onPress={() => this.showImageBAST(imageBase64Bast, key)}>
                    <Text style={{ fontSize: 10 }}>{key + 1}</Text>
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
        })
    }

    removePinjam(index) {
        this.setState((prevState) => ({
            imagePinjamArray: prevState.imagePinjamArray.filter((_, i) => i !== index)
        }));
        this.setState({
            isModalImagePinjam: false
        })
    }

    multipleImagePinjam() {
        return this.state.imagePinjamArray.map((item, key) => {
            var imageBase64Pinjam = 'data:image/png;base64,' + item.imagebase64;
            return (
                <TouchableHighlight
                    style={styles.noPhoto}
                    onPress={() => this.showImagePinjam(imageBase64Pinjam, key)}>
                    <Text style={{ fontSize: 10 }}>{key + 1}</Text>
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
        })
    }

    showImageBAST(imageBAST, index) {
        this.setState({
            imagePopupBAST: imageBAST,
            isModalImageBAST: true,
            removeImagePopupBASTindex: index
        })
    }

    showImagePinjam(imagePinjam, index) {
        this.setState({
            imagePopupPinjam: imagePinjam,
            isModalImagePinjam: true,
            removeImagePopupPinjamindex: index
        })
    }

    onChangeDefectList(remarks, parentId, subId) {
        let arrRemarks = [...this.state.exRoom];
        arrRemarks[parentId].form[subId].remarks = remarks
        this.setState({
            exRoom: arrRemarks,

        })
    }

    onChangeRemarks(remarks, parentId, subId) {
        let arrRemarks = [...this.state.exRoom];
        arrRemarks[parentId].form[subId].remarks = remarks
        this.setState({
            exRoom: arrRemarks
        })
    }

    loadDetailHandover = () => {
        const { bastDetail } = this.state;
        axios.get(UrlApi.GET_HANDOVER_DETAIL + this.params.invitationID, {
            headers: {
                'content-type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            }
        })
            .then((response) => {
                // alert(JSON.stringify(response.data.result.data.length))

                if (response.data.result.status) {
                    this.setState({
                        data: response.data.result.data,
                        detail: response.data.result.data.detail,
                    })
                    // alert(JSON.stringify(this.state.data.detail))
                    let data = this.state.data ? this.state.data.bastDetail : [];
                    let imageBASTbase64 = []

                    let dataDetail = this.state.data ? this.state.data.detail : [];
                    let detailDefect = []


                    for (let i = 0; i < data.length; i++) {
                        imageBASTbase64.push({ imagebase64: data[i].imagebase64 })

                    }

                    for (let i = 0; i < dataDetail.length; i++) {

                        detailDefect.push({
                            roomID: dataDetail[i].roomID,
                            imagebase64: dataDetail[i].imagebase64,
                            defectID: dataDetail[i].defectID,
                            remarks: dataDetail[i].remarks

                        })


                    }
                    this.setState({
                        isCheckedBast: this.state.data.isTTDBAST,
                        isCheckedSerah: this.state.data.isSerahterimaKunci,
                        isCheckedSmartKey: this.state.data.isSmartKey,
                        isCheckedHandover: this.state.data.isHandoverKit,
                        isCheckedTata: this.state.data.isTatatertib,
                        isCheckedPinjam: this.state.data.isDPP,
                        imagePinjamArray: this.state.data.dppDetail,
                        bastNo: this.state.data.bastNo,
                        bastDate: this.state.data.bastDate,
                        isCheckedBonus: this.state.data.isBonus,
                        bonusDescription: this.state.data.bonusDescription,
                        // imageBASTArray: this.state.data.bastDetail,
                        imageBASTArray: imageBASTbase64,
                        imagePinjamArray: this.state.data.dppDetail,
                        detailDefectList: detailDefect,
                        isLoading: false
                    })

                    this.loadRoom();
                } else {
                    this.setState({
                        isLoading: false
                    })
                    alert(response.data.result.message)
                }
            })
            .catch((error) => {
                console.log(error);
                Alert.alert('Warning', error.response.data.error.message,
                    [{
                        text: 'OK',
                        onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                    }],
                    { cancelable: false }
                );
            });
    }

    showImageDefect(imageDefect) {
        this.setState({
            imagePopupDefect: imageDefect,
            isModalImageDefect: true,
        })
    }

    editHandover() {


        let detailNew = []
        let detailNull = []
        this.state.exRoom.map((item) => {
            item.form.map((val) => {
                
                if (val.remarks == '-' || val.imagebase64 == '') {
                    detailNull.push({
                        roomID: val.roomID,
                        remarks: val.remarks,
                        imagebase64: val.imagebase64

                    })

                } else {
                    detailNew.push({
                        roomID: val.roomID,
                        remarks: val.remarks,
                        imagebase64: val.imagebase64,

                    })

                }
            })

        })

        this.setState({
            isLoadingButton: true
        })

        console.log('Detail ', this.state.detailDefectList)

        if (this.params.statusPaymentCode != 'LNS') {
            if (this.state.isCheckedBast == false && this.state.imageBASTArray.length == 0 && this.state.isCheckedSerah == false && this.state.isCheckedSmartKey == false && this.state.isCheckedHandover == false && this.state.isCheckedTata == false && this.state.isCheckedPinjam == false && this.state.imagePinjamArray.length == 0 && (detailNull.length > 0 || detailNew.length == 0)) {
                Alert.alert(
                    'Warning',
                    'Please input some checklist or some defectlist first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (this.state.isCheckedBast == true && this.state.imageBASTArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Tandatangan Bast first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (this.state.isCheckedBast == true && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Insert BAST No first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedPinjam == true && this.state.imagePinjamArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Dokument Pinjam Pakai first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (detailNull.length != 0) {
                Alert.alert(
                    'Warning',
                    'Please complete your defect input!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else {
                Alert.alert(
                    'Info',
                    'Are you sure you want to save changes?',
                    [
                        { text: 'Cancel', style: 'cancel' },
                        { text: 'OK', onPress: () => this.sendRequest(detailNew) },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            }
        } else {
            if (this.state.isCheckedBast == false && this.state.imageBASTArray.length == 0 && this.state.isCheckedSerah == false && this.state.isCheckedSmartKey == false && this.state.isCheckedHandover == false && this.state.isCheckedTata == false && (detailNull.length > 0 || detailNew.length == 0)) {
                Alert.alert(
                    'Warning',
                    'Please input some checklist or some defectlist first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (this.state.isCheckedBast == true && this.state.imageBASTArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Tandatangan Bast first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (this.state.isCheckedBast == true && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Insert BAST No first!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            } else if (detailNull.length != 0) {
                Alert.alert(
                    'Warning',
                    'Please complete your defect input!',
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            }
            else {
                Alert.alert(
                    'Info',
                    'Are you sure you want to save changes?',
                    [
                        { text: 'Cancel', style: 'cancel' },
                        { text: 'OK', onPress: () => this.sendRequest(detailNew) },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoadingButton: false
                })
            }
        }
    }

    
    sendRequest(detailNew) {
        this.setState({
            isLoadingButton: true
        })

        if (detailNew.length === 0) {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {


                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        } else {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailDPP: this.state.imagePinjamArray,
                        detail: this.state.detailDefectList
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detail: this.state.detailDefectList
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray,
                        detail: this.state.detailDefectList
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detail: this.state.detailDefectList
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.put(UrlApi.UPDATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detail: this.state.detailDefectList
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                                Alert.alert(
                                    'Success',
                                    'Update Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        }
    }

    render() {
        return (
            <View style={{
                backgroundColor: '#EFEFEF', flex: 1,
                alignItems: "center"
            }}>
                {this.state.isLoading == false ?
                    <ScrollView style={{ flex: 1, width: '100%' }}>
                        <View style={styles.titleContainer}>
                            <Text
                                style={styles.title}
                            >Detail Customer</Text>
                        </View>
                        <View style={styles.detailCustomerContainer}>
                            <View style={styles.containerItem}>
                                <View style={styles.item50}>
                                    <Text>
                                        Customer Name
                                </Text>
                                </View>
                                <View style={styles.item50}>
                                    <Text>
                                        :  {this.params.psName}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.containerItem}>
                                <View style={styles.item50}>
                                    <Text>
                                        Project
                                </Text>
                                </View>
                                <View style={styles.item50}>
                                    <Text>
                                        :  {this.params.project}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.containerItem}>
                                <View style={styles.item50}>
                                    <Text>
                                        Unit Code
                                </Text>
                                </View>
                                <View style={styles.item50}>
                                    <Text>
                                        :  {this.params.unitcode}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.containerItem}>
                                <View style={styles.item50}>
                                    <Text>
                                        Unit No
                                </Text>
                                </View>
                                <View style={styles.item50}>
                                    <Text>
                                        :  {this.params.unitno}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.titleContainer}>
                            <Text
                                style={styles.title}
                            >Checklist</Text>
                        </View>
                        <View style={styles.detailCustomerContainer}>
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <View style={styles.containerItemTwo}>
                                        <View>
                                            <Text>
                                                Tandatangan BAST
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.item5}>
                                    <CheckBox
                                        disabled={this.state.data.isTTDBAST == true ? true : false}
                                        value={this.state.isCheckedBast}
                                        onChange={() => this.checkBoxBastOnChage()}>
                                    </CheckBox>
                                </View>
                                <TouchableHighlight style={styles.item20}
                                    onPress={() =>
                                        this.state.isCheckedBast == true ?
                                            this.takePhotoBAST(false) : this.alertMessage('Tandatangan BAST')}
                                    activeOpacity={1}>
                                    <Image
                                        source={require('../assets/camera.png')}
                                        style={{ height: 20, width: 20, resizeMode: 'contain', marginTop: 5 }}>
                                    </Image>
                                </TouchableHighlight>
                                {this.state.isCheckedBast == true ? <View style={styles.item30}>
                                    <TextInput
                                        onChangeText={(bastNo) => this.setState({ bastNo: bastNo })}
                                        value={this.state.bastNo}
                                        editable={this.state.isCheckedBast == true ? true : false}
                                        maxLength={500}
                                        style={styles.textInput}>
                                    </TextInput>
                                </View> : <View />}
                                {/* {this.state.imageBASTbase64 == '' || this.state.imageBASTbase64 == null ? <View/> : 
                            <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={{uri: imageBase64BAST}}/>
                            } */}
                            </View>
                            <View style={styles.containerItemTwo}>
                                {this.multipleImageBast()}
                            </View>
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <View style={styles.containerItemTwo}>
                                        <View>
                                            <Text>
                                                Serah Terima Kunci
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.item5}>
                                    <CheckBox
                                        disabled={this.state.data.isSerahterimaKunci == true ? true : false}
                                        value={this.state.isCheckedSerah}
                                        onChange={() => this.checkBoxSerahOnChage()}>
                                    </CheckBox>
                                </View>
                            </View>
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <View style={styles.containerItemTwo}>
                                        <View>
                                            <Text>
                                                Serah Terima Smart Key
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.item5}>
                                    <CheckBox
                                        disabled={this.state.data.isSmartKey == true ? true : false}
                                        value={this.state.isCheckedSmartKey}
                                        onChange={() => this.checkBoxSmartKeyOnChage()}>
                                    </CheckBox>
                                </View>
                            </View>
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <View style={styles.containerItemTwo}>
                                        <View>
                                            <Text>
                                                Handover Kit
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.item5}>
                                    <CheckBox
                                        disabled={this.state.data.isHandoverKit == true ? true : false}
                                        value={this.state.isCheckedHandover}
                                        onChange={() => this.checkBoxHandoverOnChage()}>
                                    </CheckBox>
                                </View>
                            </View>
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <View style={styles.containerItemTwo}>
                                        <View>
                                            <Text>
                                                Tata Tertib Sarusun
                                        </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.item5}>
                                    <CheckBox
                                        disabled={this.state.data.isTatatertib == true ? true : false}
                                        value={this.state.isCheckedTata}
                                        onChange={() => this.checkBoxTataOnChage()}>
                                    </CheckBox>
                                </View>
                            </View>
                            {this.params.statusPaymentCode != 'LNS' ?
                                <View>
                                    <View style={styles.containerItemTwo}>
                                        <View style={styles.item50}>
                                            <View style={styles.containerItemTwo}>
                                                <View>
                                                    <Text>
                                                        Dok. Pinjam Pakai
                                            </Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.item5}>
                                            <CheckBox
                                                disabled={this.state.data.isDPP == true ? true : false}
                                                value={this.state.isCheckedPinjam}
                                                onChange={() => this.checkBoxPinjamOnChage()}>
                                            </CheckBox>
                                        </View>
                                        <TouchableHighlight style={styles.item20}
                                            onPress={() =>
                                                this.state.isCheckedPinjam == true ?
                                                    this.takePhotoPinjam(false) : this.alertMessage('Dokument Pinjam Pakai')}
                                            activeOpacity={1}>
                                            <Image
                                                source={require('../assets/camera.png')}
                                                style={{ height: 20, width: 20, resizeMode: 'contain', marginTop: 5 }}>
                                            </Image>
                                        </TouchableHighlight>
                                        {/* {this.state.imagePinjambase64 == '' || this.state.imagePinjambase64 == null ? <View/> : 
                                    <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Pinjam}}/>
                                    } */}
                                    </View>
                                    <View style={styles.containerItemTwo}>
                                        {this.multipleImagePinjam()}
                                    </View>
                                </View> : <View />}
                            <View style={styles.containerItemTwo}>
                                <View style={styles.item50}>
                                    <Text>
                                        Bonus
                                </Text>
                                </View>
                                <View style={styles.item20}>
                                    <CheckBox
                                        disabled={this.state.data.isBonus == true ? true : false}
                                        value={this.state.isCheckedBonus}
                                        onChange={() => this.checkBoxBonusOnChage()}>
                                    </CheckBox>
                                </View>
                                <View style={styles.item40}>
                                    <TextInput
                                        onChangeText={(bonusDescription) => this.onChangeBonus(bonusDescription)}
                                        value={this.state.isCheckedBonus ? this.state.bonusDescription : null}
                                        maxLength={500}
                                        style={styles.textInput}
                                        autoCapitalize='characters'
                                        editable={this.state.isCheckedBonus == true ? true : false}></TextInput>
                                </View>
                            </View>
                        </View>
                        <View style={styles.titleContainer}>
                            <Text
                                style={styles.title}
                            >Defect List</Text>
                        </View>
                        {/* <View style={styles.detailCustomerContainer}>
                        {this.state.isLoadingDefect == true ? <ActivityIndicator/>: this.renderRoomDefect()}
                    </View> */}
                        <View style={styles.detailCustomerContainer}>
                            {this.state.detail.length == 0 ? <Text>No Defect List</Text> : this.renderRoomDefect()}
                        </View>
                        <View style={{
                            margin: 10,
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            {this.state.isLoadingButton == false ?
                                <Button
                                    title="UPDATE"
                                    color="#CB2406"
                                    onPress={() =>
                                        this.editHandover()
                                        // console.log('BAST IMAGE ARRAY',this.state.imageBASTArray)
                                    }
                                /> : <ActivityIndicator />}
                        </View>
                    </ScrollView> :
                    <ActivityIndicator />}
                <Modal
                    isOpen={this.state.isModalImageDefect}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 380,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImageDefect: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupDefect }} />
                </Modal>
                <Modal
                    isOpen={this.state.isModalImageBAST}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 405,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImageBAST: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupBAST }} />
                    <Text
                        style={{ textAlign: 'center', marginTop: 7, color: '#CB2406', textDecorationLine: 'underline' }}
                        onPress={() => this.removeBAST(this.state.removeImagePopupBASTindex)}
                    >Delete</Text>
                </Modal>
                <Modal
                    isOpen={this.state.isModalImagePinjam}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 405,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImagePinjam: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupPinjam }} />
                    <Text
                        style={{ textAlign: 'center', marginTop: 7, color: '#CB2406', textDecorationLine: 'underline' }}
                        onPress={() => this.removePinjam(this.state.removeImagePopupPinjamindex)}
                    >Delete</Text>
                </Modal>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    titleContainer: {
        borderBottomWidth: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: 'white',
        marginTop: 10,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 5,
    },

    detailCustomerContainer: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 5,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginBottom: 5
    },

    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 8
    },

    containerItem: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item50: {
        width: '40%',
    },

    containerItemTwo: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item20: {
        width: '10%',
        marginTop: -5,
    },

    item5: {
        width: '5%',
        marginTop: -5,
        marginRight: 15,
    },

    item30: {
        width: '30%',
    },

    item40: {
        width: '40%',
    },

    textInput: {
        borderWidth: 1,
        borderColor: '#cecece',
        height: 25,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        fontSize: 11,
        paddingVertical: 2,
        paddingHorizontal: 6
    },

    cardDefect: {
        margin: 5,
        padding: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderWidth: 1,
        borderColor: '#CB2406',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    marginTop: {
        marginTop: 5
    },
    noPhoto: {
        backgroundColor: '#DDD9D9',
        paddingHorizontal: 5,
        paddingVertical: 5,
        width: '7%',
        marginTop: 3,
        marginBottom: 3,
        marginRight: 3,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    }
})