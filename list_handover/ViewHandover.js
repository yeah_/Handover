import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  Text, 
  View,
  Image,
  BackHandler,
  TouchableHighlight,
  CheckBox,
  TextInput,
  ActivityIndicator,
  AsyncStorage,
  Alert } from 'react-native';
  import UrlApi from '../config/UrlApi';
  import axios from 'axios';
  import Modal from 'react-native-modalbox';
  import { Dropdown } from 'react-native-material-dropdown';
  import _ from 'lodash'

  export default class ViewHandover extends Component {
    
    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            valueArray: [], 
            data:[],
            detail:[],
            disabled: false,
            isModalImageBAST : false,
            isModalImagePinjam : false,
            isModalImageDefect : false,
            imagePopupDefect: '',
            imagePopupBAST: '',
            imagePopupPinjam: '',
            exRoom: [],
            defectList: [],
            token: ''
        }
        this.params = this.props.navigation.state.params;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    static navigationOptions = {
        title : "View Hand Over"
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('ListHandoverScreen')
        return true;
    }

    alertMessage(message){
        Alert.alert(
            'Warning',
            'Please checked the '+message+' checkist first!',
          )
    }

    componentDidMount(){
    }

    componentWillMount(){
        this.getToken()
        this.props.navigation.addListener('didFocus', () => {
            this.loadDetailHandover();
        });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async getToken(){
        try{ 
            let token = await AsyncStorage.getItem('accessToken');
            return this.setState({
                token : token
            })
        }
        catch(e){
            console.log('caught error', e);
            // Handle exceptions
        }
    
    }

    showImageBAST(imageBAST){
        this.setState({
            imagePopupBAST: imageBAST,
            isModalImageBAST : true,
        })
    }

    showImagePinjam(imagePinjam){
        this.setState({
            imagePopupPinjam: imagePinjam,
            isModalImagePinjam : true,
        })
    }

    showImageDefect(imageDefect){
        this.setState({
            imagePopupDefect: imageDefect,
            isModalImageDefect : true,
        })
    }

    loadRoom(){
        axios.get(UrlApi.GET_ROOM_LIST, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
            .then((response) => {
                if (response.data.result.status) {
                this.setState({
                    exRoom : response.data.result.data
                })
                const room = this.state.exRoom.map((room)=>{
                    return {...room, form:[], defectList:[]}
                })
                this.setState({
                    exRoom: room
                })
                // alert(JSON.stringify(this.state.exRoom))
                    var form = [...this.state.exRoom]
                    var formFix = []
                    var defectList = []
                    this.state.detail.map((item)=>{                        
                        this.state.exRoom.map((val, i)=>{
                            axios.get(UrlApi.GET_DEFECT_LIST+item.roomID, {
                                headers:{
                                    'content-type': 'application/json',
                                    'Authorization': 'Bearer '+this.state.token
                                }
                                }).then((response) => {
                                    if (response.data.result.status) {
                                            response.data.result.data.map((item)=>{
                                                defectList.push({
                                                    id: item.id,
                                                    name: item.name
                                                })
                                            })
                                            if(item.roomID == val.id){
                                                form[i].form.push({
                                                    roomID: item.roomID,
                                                    remarks: item.remarks,
                                                    imagebase64: item.imagebase64
                                                })
                                                defectList.map((res)=>{
                                                    form[i].defectList.push({
                                                        id: res.id,
                                                        name: res.name
                                                    })
                                                })
                                            }
                                            this.setState({
                                                exRoom : form
                                            }) 
                                            this.state.exRoom.map((item)=>
                                            {
                                                if(item.form.length != 0 || item.defectList.length!=0){
                                                    formFix.push({
                                                        id: item.id,
                                                        name: item.name,
                                                        form: item.form,
                                                        defectList: item.defectList
                                                    })
                                                }
                                            })

                                            const values = 
                                                _.chain(formFix)
                                                .uniqWith(_.isEqual)
                                                .value()

                                            this.setState({
                                                exRoom : values,
                                            })
                                    }
                                    // alert(JSON.stringify(this.state.exRoom))
                                }).catch((error)=> {
                                console.log(error);
                                // alert(JSON.stringify(error))
                                Alert.alert('Warning',error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.navigate('ListHandoverScreen')}],
                                    {cancelable: false}
                                );
                                });
                        })
                    })
                }
                this.setState({
                    isLoading: false
                })
            })
            .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.props.navigation.navigate('ListHandoverScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadDetailHandover(){
        axios.get(UrlApi.GET_HANDOVER_DETAIL+this.params.invitationID, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
              .then((response) => {
                  // alert(JSON.stringify(response.data.result.data.length))
                  if (response.data.result.status) {
                        this.setState({
                            data : response.data.result.data,
                            detail: response.data.result.data.detail,
                        })
                        this.loadRoom()
                  } else {
                    this.setState({
                      isLoading: false
                    })
                      alert(response.data.result.message)
                  }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.props.navigation.navigate('ListHandoverScreen')}],
                  {cancelable: false}
                );
            });
    }

    renderRoomDefect(){ 
        return this.state.exRoom.map((item, key) => {
            return (
                <View>
                    <View style={styles.containerItem}>
                        <View style={{width: '10%'}}>
                            <Image
                                style={{width: 20, height: 20, resizeMode: 'contain'}}
                                source={require('../assets/placeholder.png')}
                            />
                        </View>
                        <View style={{width: '90%'}}>
                            <Text style={{color: '#CB2406', fontWeight:'bold'}}>{item.name}</Text>
                        </View>
                    </View>
                    <View style={{marginTop: 5}}>
                        {item.form ? item.form.map((val, i)=>{
                        var imageBase64Defect = 'data:image/png;base64,'+val.imagebase64;
                            return(
                                <View style={styles.cardDefect}>
                                    <TouchableHighlight
                                        onPress={()=>this.showImageDefect(imageBase64Defect)}>
                                            {val.imagebase64 == '' ? <View/> :
                                            <Image style={{width: 40, height: 40, resizeMode: 'contain', marginLeft: 5}} source={{uri: imageBase64Defect}}/>}
                                    </TouchableHighlight>
                                    <Dropdown
                                        containerStyle={{marginHorizontal: 10, marginTop:-20, width: val.imagebase64 == '' ? '100%' : '70%'}}
                                        label='Remarks'
                                        data={item.defectList}
                                        value={val.remarks}
                                        fontSize={12}
                                        valueExtractor={({id})=> id}
                                        labelExtractor={({name})=> name}
                                        disabled={true}
                                        itemColor='#000' 
                                        selectedItemColor='#BB0A0A'
                                    />
                                    {/* <TextInput
                                    editable={false}
                                        style={{borderColor: '#cecece' ,borderWidth: 1, marginLeft: 10, width: val.imagebase64 == '' ? '100%' : '75%'}}
                                        maxLength={500}
                                        value={val.remarks || ''}/> */}
                                </View> 
                            );
                        }):<View/>} 
                    </View>
                </View>
            )
        })
    }

    multipleImageBast(){
        return this.state.data.bastDetail ? this.state.data.bastDetail.map((item, key) => { 
            var imageBase64Bast = 'data:image/png;base64,'+item.imagebase64;
            return (
                <TouchableHighlight
                style={styles.noPhoto}
                onPress={()=>this.showImageBAST(imageBase64Bast)}>
                    <Text style={{fontSize: 10}}>{key+1}</Text> 
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
          }):<View/>
    } 
 
    multipleImagePinjam(){
        return this.state.data.dppDetail ? this.state.data.dppDetail.map((item, key) => {
            var imageBase64Pinjam = 'data:image/png;base64,'+item.imagebase64;
            return (
                <TouchableHighlight
                style={styles.noPhoto}
                onPress={()=>this.showImagePinjam(imageBase64Pinjam)}>
                    <Text style={{fontSize: 10}}>{key+1}</Text> 
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
          }) :<View/>
    }

    render(){
        return(
            <View style={{backgroundColor:'#EFEFEF', flex:1, 
            alignItems: "center"}}>
            {this.state.isLoading == false ?
                <ScrollView style={{flex:1, width:'100%'}}>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                            >Detail Customer</Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Customer Name
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.psName}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Project
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.project}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Unit Code
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.unitcode}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Unit No
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.unitno}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                            >Checklist</Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Tandatangan BAST
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                value={this.state.data.isTTDBAST}
                                disabled={true}>
                                </CheckBox>
                            </View>
                            {this.state.data.bastNo == '' || this.state.data.bastNo == null ? <View/> : 
                            <View style={styles.item40}>
                                <TextInput
                                    value={this.state.data.bastNo}
                                    editable={false}
                                    style={styles.textInput}>
                                </TextInput>
                            </View>} 
                        </View>
                        <View style={styles.containerItemTwo}>
                            {this.multipleImageBast()} 
                        </View> 
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Serah Terima Kunci 
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                disabled={true}
                                value={this.state.data.isSerahterimaKunci}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Serah Terima Smart Key
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                disabled={true}
                                value={this.state.data.isSmartKey}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Handover Kit
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                disabled={true}
                                value={this.state.data.isHandoverKit}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Tata Tertib Sarusun
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                disabled={true}
                                value={this.state.data.isTatatertib}>
                                </CheckBox>
                            </View>
                        </View>
                        {this.params.statusPaymentCode != 'LNS' ?
                            <View>
                                <View style={styles.containerItemTwo}>
                                    <View style={styles.item50}>
                                        <View style={styles.containerItemTwo}>
                                            <View>
                                                <Text>
                                                    Dok. Pinjam Pakai
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.item5}>
                                        <CheckBox
                                        value={this.state.data.isDPP}
                                        disabled={true}>
                                        </CheckBox>
                                    </View> 
                                </View>
                                <View style={styles.containerItemTwo}>
                                    {this.multipleImagePinjam()}
                                </View> 
                            </View>  : <View/>}
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Bonus
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                disabled={true}
                                value={this.state.data.isBonus}>
                                </CheckBox>
                            </View>
                            <View style={styles.item40}>
                                <TextInput
                                value={this.state.data.bonusDescription}
                                style={styles.textInput}
                                autoCapitalize='characters'
                                editable={false}></TextInput>
                            </View>
                        </View>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                            >Defect List
                        </Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        {this.state.detail.length == 0 ? <Text>No Defect List</Text> : this.renderRoomDefect()}
                    </View>
                 </ScrollView> : <ActivityIndicator/>}
            <Modal
                isOpen={this.state.isModalImageBAST}
                style={{
                    justifyContent:'center',
                    borderRadius:30,
                    shadowRadius: 10,
                    width: 340,
                    height: 405,
                    padding: 10
                }}
                position='center'
                backdrop={true}
                onClosed={()=>this.setState({
                    isModalImageBAST: false
                })}>
                <Image style={{height:370, resizeMode: 'contain'}} source={{uri: this.state.imagePopupBAST}}/>
            </Modal>
            <Modal
                isOpen={this.state.isModalImagePinjam}
                style={{
                    justifyContent:'center',
                    borderRadius:30,
                    shadowRadius: 10,
                    width: 340,
                    height: 405,
                    padding: 10
                }}
                position='center'
                backdrop={true}
                onClosed={()=>this.setState({
                    isModalImagePinjam: false
                })}>
                <Image style={{height:370, resizeMode: 'contain'}} source={{uri: this.state.imagePopupPinjam}}/>
            </Modal>
            
            <Modal
                isOpen={this.state.isModalImageDefect}
                style={{
                    justifyContent:'center',
                    borderRadius:30,
                    shadowRadius: 10,
                    width: 340,
                    height: 380,
                    padding: 10
                }}
                position='center'
                backdrop={true}
                onClosed={()=>this.setState({
                    isModalImageDefect: false
                })}>
                <Image style={{height:370, resizeMode: 'contain'}} source={{uri: this.state.imagePopupDefect}}/>
            </Modal>
        </View>
          );
    }
  }

  const styles = StyleSheet.create({
    titleContainer:{
        borderBottomWidth:1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: 'white',
        marginTop: 10,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 10,  
        elevation: 5,
    },

    detailCustomerContainer:{
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.8,
        shadowRadius: 10,  
        elevation: 5,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginBottom: 5
    },

    title:{
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 8
    }, 

    containerItem:{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item50:{
        width: '40%',
    },

    containerItemTwo:{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item20:{
        width: '10%',
        marginTop: -5,
    }, 

    item5:{
        width: '5%',
        marginTop: -5,
        marginRight: 15, 
    }, 

    item30:{
        width: '30%',
    },

    item40:{
        width: '40%',
    },

    textInput:{
        borderWidth:1,
        borderColor:'#cecece',
        height: 25,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        fontSize: 11,
        paddingVertical: 2,
        paddingHorizontal: 6
    },
 
    cardDefect :{
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        margin: 5,
        padding: 10,
        borderWidth: 1,
        borderColor: '#CB2406',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    marginTop10 : {
        marginTop: 10
    },

    marginBottom:{
        marginBottom: -15
    },

    noPhoto: {
        backgroundColor: '#DDD9D9', 
        paddingHorizontal: 5,
        paddingVertical: 5,
        width: '7%', 
        marginTop: 3,
        marginBottom: 3,
        marginRight: 3,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    }

  })