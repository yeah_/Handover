import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    Animated,
    View,
    Image,
    CheckBox,
    AsyncStorage,
    TextInput,
    Button,
    BackHandler,
    TouchableHighlight,
    ActivityIndicator,
    Alert
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import { HeaderBackButton } from 'react-navigation';
import { Dropdown } from 'react-native-material-dropdown';
import Modal from 'react-native-modalbox';
import UrlApi from '../config/UrlApi';
import axios from 'axios';
import xmlparse from 'react-xml-parser';
import dateFormat from 'dateformat'
import ImageResizer from 'react-native-image-resizer'

export default class CreateHandover extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isCheckedBast: false,
            isCheckedSerah: false,
            isCheckedSmartKey: false,
            isCheckedHandover: false,
            isCheckedTata: false,
            isCheckedBonus: false,
            isCheckedPinjam: false,
            valueArray: [],
            disabled: false,
            bonusDescription: '',
            bastNo: '',
            isLoading: false,
            isLoadingButton: false,
            isValidDefect: true,
            isModalImageDefect: false,
            isModalImageBAST: false,
            isModalImagePinjam: false,
            imagePopupDefect: '',
            imagePopupBAST: '',
            imagePopupPinjam: '',
            exRoom: [],
            BASTdate: '',
            token: '',
            imageBASTArray: [],
            imagePinjamArray: [],
            defectList: [],
            defectListVal: [],
            detailDefect: [],
            removeImagePopupBASTindex: 0,
            removeImagePopupPinjamindex: 0,
            defectID: '',
            onCreateStatus: false,
            editableTextField: true,


            arrayAPILK: [],
            siteID: '',
            orgID: '',
            unitCode: '',
            unitNo: '',
            coCode: '',
            bookCode: '',
            psCode: '',
            name: '',
            bastType: '',
            stType: '',
            remarks: '',
            username: '',
            requestorName: '',
            requestorPhone: '',
            requestorEmail: '',
            type: '',
            fileType: '',
            handoverDetails: [],
            caseNumber: '',
            bastID: '',
        }
        this.today = new Date();
        this.params = this.props.navigation.state.params;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.index = 0;
        this.animatedValue = new Animated.Value(0);
    }


    static navigationOptions = ({ navigation }) => {
        return {
            title: "Create Hand Over",
            headerLeft: (
                <HeaderBackButton
                    color="#fff"
                    tintColor="#fff"
                    onPress={() => {
                        Alert.alert(
                            'Exit App',
                            'Are you sure you want to go back?', [
                            {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel'
                            }, {
                                text: 'OK',
                                onPress: () => navigation.navigate('ListHandoverScreen')
                            },], {
                            cancelable: false
                        }
                        )
                    }} />
            ),
        }
    }


    alertMessage(message) {
        Alert.alert(
            'Warning',
            'Please checked the ' + message + ' checklist first!',
        )
    }

    componentDidMount() {
        this.loadRoom()
        this.getToken()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    async getToken() {
        try {
            let token = await AsyncStorage.getItem('accessToken');
            return this.setState({
                token: token
            })
        }
        catch (e) {
            console.log('caught error', e);
            // Handle exceptions
        }

    }

    handleBackButtonClick() {
        Alert.alert(
            'Exit App',
            'Are you sure you want to go back?', [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('ListHandoverScreen')
            },], {
            cancelable: false
        }
        )
        return true;
    }

    checkBoxBastOnChage() {
        this.setState({
            isCheckedBast: !this.state.isCheckedBast,
            bastNo: '',
            imageBASTArray: []
        })
    }

    checkBoxPinjamOnChage() {
        this.setState({
            isCheckedPinjam: !this.state.isCheckedPinjam,
            imagePinjamArray: [],
        })
    }

    checkBoxSerahOnChage() {
        this.setState({
            isCheckedSerah: !this.state.isCheckedSerah
        })
    }

    checkBoxSmartKeyOnChage() {
        this.setState({
            isCheckedSmartKey: !this.state.isCheckedSmartKey
        })
    }

    checkBoxHandoverOnChage() {
        this.setState({
            isCheckedHandover: !this.state.isCheckedHandover
        })
    }

    checkBoxTataOnChage() {
        this.setState({
            isCheckedTata: !this.state.isCheckedTata
        })
    }

    checkBoxBonusOnChage() {
        this.setState({
            isCheckedBonus: !this.state.isCheckedBonus,
            bonusDescription: '',
        })
    }

    takePhotoBAST(cropping, mediaType = 'photo') {
        if (this.state.imageBASTArray.length == 10) {
            alert("You can only take photos 10 times!")
        } else {
            ImagePicker.openCamera({
                cropping: cropping,
                width: 250,
                height: 200,
                compressImageMaxWidth: 250,
                compressImageMaxHeight: 200,
                compressImageQuality: 0.6,
                includeExif: true,
                mediaType,
            }).then(imageBAST => {
                console.log('received image', imageBAST);
                this.setState({
                    imageBAST: { uri: imageBAST.path, width: imageBAST.width, height: imageBAST.height, mime: imageBAST.mime },
                    imagesBAST: null
                });

            

                let d = this.today.getDate();
                let m = this.today.getMonth('MM') + 1;
                let y = this.today.getFullYear();
                let mS = m.toString();
                let dS = d.toString();
                let lengthmS = mS.length;
                let lengthdS = dS.length;
                let monthValue = '';
                let dayValue = '';

                if (lengthmS == 1) {
                    monthValue = '0' + mS
                } else {
                    monthValue = mS
                }

                if (lengthdS == 1) {
                    dayValue = '0' + dS
                } else {
                    dayValue = dS
                }

                let dateString = y + '-' + monthValue + '-' + dayValue
                this.setState({
                    BASTdate: dateString
                })

                ImgToBase64.getBase64String(imageBAST.path)
                    .then(base64String => {
                        var imageBASTArray = this.state.imageBASTArray;
                        imageBASTArray.push({
                            imagebase64: base64String
                        })
                        this.setState({
                            imageBASTArray: imageBASTArray
                        })
                    })
                    .catch(err => console.log(err));

               
            }).catch(e => console.log(e));
        }
    }

    takePhotoPinjam(cropping, mediaType = 'photo') {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 250,
            height: 200,
            compressImageMaxWidth: 250,
            compressImageMaxHeight: 200,
            compressImageQuality: 0.7,
            includeExif: true,
            mediaType,
        }).then(imagePinjam => {
            console.log('received image', imagePinjam);
            this.setState({
                imagePinjam: { uri: imagePinjam.path, width: imagePinjam.width, height: imagePinjam.height, mime: imagePinjam.mime },
                imagePinjam: null
            });
            ImgToBase64.getBase64String(imagePinjam.path)
                .then(base64String => {
                    var imagePinjamArray = this.state.imagePinjamArray;
                    imagePinjamArray.push({
                        imagebase64: base64String
                    })
                    this.setState({
                        imagePinjamArray: imagePinjamArray
                    })
                })
                .catch(err => console.log(err));
        }).catch(e => console.log(e));
    }

    takePhotoDefect(cropping, mediaType = 'photo', parentId, subId) {
        ImagePicker.openCamera({
            cropping: cropping,
            width: 250,
            height: 200,
            compressImageMaxWidth: 250,
            compressImageMaxHeight: 200,
            compressImageQuality: 0.6,
            includeExif: true,
            mediaType,
        }).then(imageDefect => {
            console.log('received image defect', imageDefect);
            this.setState({
                imageDefect: { uri: imageDefect.path, width: imageDefect.width, height: imageDefect.height, mime: imageDefect.mime },
                imagesDefect: null,
            }
            );

            ImgToBase64.getBase64String(imageDefect.path)
            .then(base64String => {
                let exRoomNew = [...this.state.exRoom]
                exRoomNew[parentId].form[subId].imagebase64 = base64String
                this.setState({
                    exRoom: exRoomNew,
                })
            })
            .catch(err => console.log(err));

            
        }).catch(e => console.log(e));
    }

    loadRoom() {
        axios.get(UrlApi.GET_ROOM_LIST, {
            headers: {
                'content-type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            }
        })
            .then((response) => {
                if (response.data.result.status) {
                    this.setState({
                        exRoom: response.data.result.data
                    })
                    const room = this.state.exRoom.map((room) => {
                        return { ...room, form: [], defectList: [] }
                    })
                    this.setState({
                        exRoom: room
                    })
                    // alert(JSON.stringify(this.state.exRoom))
                }
            })
            .catch((error) => {
                console.log(error);
                Alert.alert('Warning', error.response.data.error.message,
                    [{
                        text: 'OK',
                        onPress: () => this.navigate('ListHandoverScreen')
                    }],
                    { cancelable: false }
                );
            });
    }

    renderRoomDefect() {
        const animationValue = this.animatedValue.interpolate(
            {
                inputRange: [0, 1],
                outputRange: [-59, 0]
            });
        return this.state.exRoom.map((item, key) => {
            return (
                <View>
                    <View style={styles.containerItem}>
                        <View style={{ width: '10%' }}>
                            <TouchableHighlight
                                disabled={this.state.onCreateStatus}
                                onPress={() => this.addDefect(key, item.id)}>

                                <Image
                                    style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                    source={require('../assets/square-add-button.png')}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={{ width: '90%' }}>
                            <Text>{item.name}</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 15 }}>
                        {item.form ? item.form.map((val, i) => {
                            var imageBase64Defect = 'data:image/png;base64,' + val.imagebase64;
                            return (
                                <Animated.View i={i} style={[{ opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
                                    <View style={styles.cardDefect}>
                                        <View style={{ width: '10%' }}>
                                            <TouchableHighlight
                                                disabled={this.state.onCreateStatus}
                                                onPress={() => this.removeDefect(key, i)}>
                                                <Image
                                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                                    source={require('../assets/minus.png')}
                                                />
                                            </TouchableHighlight>
                                        </View>
                                        <TouchableHighlight style={{
                                            width: '10%',
                                            marginLeft: 3,
                                        }}
                                            disabled={this.state.onCreateStatus}
                                            onPress={() => this.takePhotoDefect(false, '', key, i)}>
                                            <Image
                                                source={require('../assets/camera.png')}
                                                style={{ marginTop: -3, height: 25, width: 25, resizeMode: 'contain' }}>
                                            </Image>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => this.showImageDefect(imageBase64Defect)}>
                                            {val.imagebase64 == '' ? <View /> :
                                                <Image style={{ width: 40, height: 40, resizeMode: 'contain', marginLeft: 10 }} source={{ uri: imageBase64Defect }} />}
                                        </TouchableHighlight>
                                        <Dropdown
                                            containerStyle={{ marginHorizontal: 10, marginTop: -20, width: val.imagebase64 == '' ? '70%' : '53%' }}
                                            label='Remarks'
                                            fontSize={12}
                                            data={item.defectList}
                                            value={val.remarks}
                                            disabled={this.state.onCreateStatus}
                                            onChangeText={(id, index) => this.onChangeDefectList(id, key, i, item.defectList[index].name, item.defectList)}
                                            valueExtractor={({ id }) => id}
                                            labelExtractor={({ name }) => name}
                                            itemColor='#000'
                                            selectedItemColor='#BB0A0A'
                                        />
                                        {/* <TextInput
                                        style={{borderWidth: 1, marginLeft: 10, width: val.imagebase64 == '' ? '75%' : '55%'}}
                                        onChangeText={remarks => this.onChangeRemarks(remarks, key,i)}
                                        maxLength={500}
                                        value={val.remarks || ''}/> */}
                                    </View>
                                </Animated.View>
                            );
                        }) : <View />}
                    </View>
                </View>
            )
        })
    }

    addDefect(index, roomID) {

        this.animatedValue.setValue(0);
        let arrDetail = [...this.state.exRoom];
        let arrRoomId = arrDetail[index].form;
        let arrDefectList = arrDetail[index].defectList;

        arrRoomId.push({
            roomID: roomID,
            imagebase64: '',
            remarks: '-'
        })

        this.setState({ disabled: true, valueArray: arrRoomId }, () => {
            Animated.timing(
                this.animatedValue,
                {
                    toValue: 1,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start(() => {
                this.setState({ disabled: false, isValidDefect: true });
            });
        });

        axios.get(UrlApi.GET_DEFECT_LIST + roomID, {
            headers: {
                'content-type': 'application/json',
                'Authorization': 'Bearer ' + this.state.token
            }
        })
            .then((response) => {
                if (response.data.result.status) {
                    if (arrDefectList.length != response.data.result.data.length) {
                        arrDefectList.push({
                            id: '-',
                            name: 'Nothing Selected'
                        })
                        response.data.result.data.map((item) => {
                            arrDefectList.push({
                                id: item.id,
                                name: item.name
                            })
                        })
                    }
                }
            })
            .catch((error) => {
                console.log(error);
                // alert(JSON.stringify(error))
                Alert.alert('Warning', error.response.data.error.message,
                    [{
                        text: 'OK',
                        onPress: () => this.navigate('ListHandoverScreen')
                    }],
                    { cancelable: false }
                );
            });

        //  alert(JSON.stringify(this.state.exRoom))
        // alert(JSON.stringify(this.state.valueArray))
    }

    removeDefect(parentId, subId) {
        let exRoomNew = [...this.state.exRoom]
        exRoomNew[parentId].form.splice(subId, 1);
        this.setState({
            exRoom: exRoomNew

        })
    }

    removeBAST(index) {
        this.setState((prevState) => ({
            imageBASTArray: prevState.imageBASTArray.filter((_, i) => i !== index)
        }));
        this.setState({
            isModalImageBAST: false
        })
    }

    multipleImageBast() {
        return this.state.imageBASTArray.map((item, key) => {
            var imageBase64Bast = 'data:image/png;base64,' + item.imagebase64;
            return (
                <TouchableHighlight
                    style={styles.noPhoto}
                    onPress={() => this.showImageBAST(imageBase64Bast, key)}>
                    <Text style={{ fontSize: 10 }}>{key + 1}</Text>
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
        })
    }

    removePinjam(index) {
        this.setState((prevState) => ({
            imagePinjamArray: prevState.imagePinjamArray.filter((_, i) => i !== index)
        }));
        this.setState({
            isModalImagePinjam: false
        })
    }

    multipleImagePinjam() {
        return this.state.imagePinjamArray.map((item, key) => {
            var imageBase64Pinjam = 'data:image/png;base64,' + item.imagebase64;
            return (
                <TouchableHighlight
                    style={styles.noPhoto}
                    onPress={() => this.showImagePinjam(imageBase64Pinjam, key)}>
                    <Text style={{ fontSize: 10 }}>{key + 1}</Text>
                </TouchableHighlight>
                // <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
            )
        })
    }

    showImageBAST(imageBAST, index) {
        this.setState({
            imagePopupBAST: imageBAST,
            isModalImageBAST: true,
            removeImagePopupBASTindex: index
        })
    }

    showImagePinjam(imagePinjam, index) {
        this.setState({
            imagePopupPinjam: imagePinjam,
            isModalImagePinjam: true,
            removeImagePopupPinjamindex: index
        })
    }

    onChangeDefectList(remarks, parentId, subId, desc, defectlist) {

        console.log('Remarks ', desc)
        this.setState({
            remarks: remarks,
            defectID: remarks
        })

        let arrRemarks = [...this.state.exRoom];
        arrRemarks[parentId].form[subId].remarks = remarks
        arrRemarks[parentId].form[subId].description = desc
        this.setState({
            exRoom: arrRemarks
        })

    }

    onChangeRemarks(remarks, parentId, subId) {
        let arrRemarks = [...this.state.exRoom];
        arrRemarks[parentId].form[subId].remarks = remarks
        this.setState({
            exRoom: arrRemarks
        })
    }

    showImageDefect(imageDefect) {
        this.setState({
            imagePopupDefect: imageDefect,
            isModalImageDefect: true,
        })
    }

    createHandover() {

        let detailNew = []
        let detailNull = []
        this.state.exRoom.map((item) => {

            item.form.map((val) => {
                if (val.remarks == '-' || val.imagebase64 == '') {
                    detailNull.push({
                        roomID: val.roomID,
                        remarks: val.remarks,
                        imagebase64: val.imagebase64,
                        defectID: val.remarks
                    })
                } else {
                    detailNew.push({
                        roomID: val.roomID,
                        remarks: val.remarks,
                        imagebase64: val.imagebase64,
                        defectID: val.remarks
                    }),

                        this.state.detailDefect.push({
                            roomID: val.roomID,
                            remarks: val.remarks,
                            imagebase64: val.imagebase64,
                            defectID: val.remarks,
                            description: val.description
                        })
                }

                console.log('Image Base 64 Defect ', val.imagebase64)
            })

        }

        )
        console.log('Params Detail Bast List ', this.state.imageBASTArray)
        console.log('Params Detail Defect List ', this.state.detailDefect)


        if (this.params.statusPaymentCode != 'LNS') {
            if (this.state.isCheckedBast == false && this.state.imageBASTArray.length == 0 && this.state.isCheckedSerah == false && this.state.isCheckedSmartKey == false && this.state.isCheckedHandover == false && this.state.isCheckedTata == false && this.state.isCheckedPinjam == false && this.state.imagePinjamArray.length == 0 && (detailNull.length > 0 || detailNew.length == 0)) {
                Alert.alert(
                    'Warning',
                    'Please input some checklist or some defectlist first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == true && this.state.imageBASTArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Tandatangan Bast first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == true && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Insert BAST No first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == false && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Input BAST Image And BAST No first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedPinjam == true && this.state.imagePinjamArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Dokument Pinjam Pakai first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (detailNull.length != 0) {
                Alert.alert(
                    'Warning',
                    'Please complete your defect input!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else {
                Alert.alert(
                    'Info',
                    'Are you sure you want to save?',
                    [
                        {
                            text: 'Cancel', style: 'cancel', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                        { text: 'OK', onPress: () => this.sendRequestCreateHO(detailNew) },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            }
        } else {
            if (this.state.isCheckedBast == false && this.state.imageBASTArray.length == 0 && this.state.isCheckedSerah == false && this.state.isCheckedSmartKey == false && this.state.isCheckedHandover == false && this.state.isCheckedTata == false && (detailNull.length > 0 || detailNew.length == 0)) {
                Alert.alert(
                    'Warning',
                    'Please input some checklist or some defectlist first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == true && this.state.imageBASTArray.length == 0) {
                Alert.alert(
                    'Warning',
                    'Please take a photo for Tandatangan Bast first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == true && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Insert BAST No first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (this.state.isCheckedBast == false && this.state.bastNo == '') {
                Alert.alert(
                    'Warning',
                    'Please Input BAST Image And BAST No first!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else if (detailNull.length != 0) {
                Alert.alert(
                    'Warning',
                    'Please complete your defect input!',
                    [
                        {
                            text: 'OK', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            } else {
                Alert.alert(
                    'Info',
                    'Are you sure you want to save?',
                    [
                        {
                            text: 'Cancel', style: 'cancel', onPress: () => this.setState({
                                detailDefect: []
                            })
                        },
                        { text: 'OK', onPress: () => this.sendRequestCreateHO(detailNew) },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                })
            }
        }
    }

    getParamApiLK() {
        axios.get(UrlApi.GET_PARAM_API_LK +
            "?invitationID=" + this.params.invitationID +
            "&bastDate=" + this.state.BASTdate +
            "&bastNo=" + this.state.bastNo
            , {
                headers: {
                    'content-type': 'application/json',
                    //   'Authorization': 'Bearer '+this.state.token
                }
            }).then((response) => {
                if (response.data.success) {
                    this.state.arrayAPILK.push(response.data.result);

                    this.setState({
                        BASTdate: this.state.arrayAPILK[0].bastDate.slice(0, 10),
                        bastNo: this.state.arrayAPILK[0].bastNo,
                        siteID: this.state.arrayAPILK[0].siteID,
                        orgID: this.state.arrayAPILK[0].orgID,
                        bastType: this.state.arrayAPILK[0].bastType,
                        bookCode: this.state.arrayAPILK[0].bookCode,
                        coCode: this.state.arrayAPILK[0].coCode,
                        fileType: this.state.arrayAPILK[0].fileType,
                        name: this.state.arrayAPILK[0].name,
                        psCode: this.state.arrayAPILK[0].psCode,
                        remarks: this.state.arrayAPILK[0].remarks,
                        requestorEmail: this.state.arrayAPILK[0].requestorEmail,
                        requestorName: this.state.arrayAPILK[0].requestorName,
                        requestorPhone: this.state.arrayAPILK[0].requestorPhone,
                        stType: this.state.arrayAPILK[0].stType,
                        type: this.state.arrayAPILK[0].type,
                        unitCode: this.state.arrayAPILK[0].unitCode,
                        unitNo: this.state.arrayAPILK[0].unitNo,
                        username: this.state.arrayAPILK[0].username,

                    })

                    this.getCaseNumberRequest(() => {

                        for (i = 0; i < this.state.arrayAPILK[0].handoverDetails.length; i++) {

                            this.state.handoverDetails.push({
                                caseNumber: this.state.caseNumber,
                                helpID: this.state.arrayAPILK[0].handoverDetails[i].helpID,
                                type: this.state.arrayAPILK[0].handoverDetails[i].type,
                                description: this.state.arrayAPILK[0].handoverDetails[i].description,
                                location: this.state.arrayAPILK[0].handoverDetails[i].location,
                                subLocation: this.state.arrayAPILK[0].handoverDetails[i].subLocation

                            })

                        }
                        console.log('handOverDetails = ', this.state.handoverDetails)
                        this.getInsertE3HandoverData(() => {
                            console.log('BAST ID ', this.state.bastID)
                            if (this.state.bastID != '') {

                                this.sendRequestCreateUniversal()
                            } else {
                                this.setState({
                                    isLoadingButton: false
                                })
                                Alert.alert('Warning', 'Failed GET BAST ID ',
                                    [{
                                        text: 'OK',
                                        onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                    }],
                                    { cancelable: false }
                                );
                            }
                        })

                        for (i = 0; i < this.state.detailDefect.length; i++) {

                            this.saveAttachmentRequest(i)

                        }

                    });


                    console.log('Result API LK ', this.state.arrayAPILK)


                } else {
                    console.log('Error Get Api LK ', response.data.error);
                    this.setState({
                        isLoadingButton: false
                    })
                    Alert.alert('Warning', response.data.error.toString(),
                        [{
                            text: 'OK',
                            onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                        }],
                        { cancelable: false }
                    );
                }
            })
    }

    getCaseNumberRequest(callback) {
        var response;
        var XMLParser = require('react-xml-parser');

        let xmls = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\
        <soap:Header>\
          <AuthHeader xmlns="http://tempuri.org/">\
            <domainName>KARAWACINET</domainName>\
            <userName>UsrE3Handover</userName>\
            <password>H4nd0v3rE3!</password>\
          </AuthHeader>\
        </soap:Header>\
        <soap:Body>\
          <GetCaseNumber xmlns="http://tempuri.org/">\
            <siteID>'+ this.state.siteID + '</siteID>\
          </GetCaseNumber>\
        </soap:Body>\
      </soap:Envelope>';


        axios.post('https://testflight.lippohomes.com/InternalMobileAppsServiceDev/WS_TMD.asmx?op=GetCaseNumber',
            xmls,
            {
                headers:
                {
                    'Content-Type': 'text/xml'

                }
            }).then(res => {
                var toJson = [];
                response = new XMLParser().parseFromString(res.data)
                toJson = response.getElementsByTagName('GetCaseNumberResult')

                if (toJson[0].value != "FAILED") {

                    this.setState({
                        caseNumber: toJson[0].value
                    })


                }



                console.log('Hasil getCaseNumberRequest ', res.data)
                callback();
            }).catch(err => { console.log('SOAP ERROR Get Case Number', err) });
    }

    getInsertE3HandoverData(callback) {
        var response;
        var XMLParser = require('react-xml-parser');


        let xmls = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\
        <soap:Header>\
          <AuthHeader xmlns="http://tempuri.org/">\
          <domainName>KARAWACINET</domainName>\
          <userName>UsrE3Handover</userName>\
          <password>H4nd0v3rE3!</password>\
          </AuthHeader>\
        </soap:Header>\
        <soap:Body>\
          <InsertE3HandoverData xmlns="http://tempuri.org/">\
            <orgID>'+ this.state.orgID + '</orgID>\
            <siteID>'+ this.state.siteID + '</siteID>\
            <unitCode>'+ this.state.unitCode + '</unitCode>\
            <unitNo>'+ this.state.unitNo + '</unitNo>\
            <coCode>'+ this.state.coCode + '</coCode>\
            <bookCode>'+ this.state.bookCode + '</bookCode>\
            <psCode>'+ this.state.psCode + '</psCode>\
            <ownerName>'+ this.state.name + '</ownerName>\
            <BASTNo>'+ this.state.bastNo + '</BASTNo>\
            <BASTType>'+ this.state.bastType + '</BASTType>\
            <STType>'+ this.state.stType + '</STType>\
            <BASTDate>'+ dateFormat(this.state.BASTdate, "yyyy-mm-dd") + '</BASTDate>\
            <remarks>'+ this.state.remarks + '</remarks>\
            <username>'+ this.state.username + '</username>\
            <requestorName>'+ this.state.requestorName + '</requestorName>\
            <requestorPhone>'+ this.state.requestorPhone + '</requestorPhone>\
            <requestorEmail>'+ this.state.requestorEmail + '</requestorEmail>\
            <jsonCaseCollection>'+ JSON.stringify(this.state.handoverDetails) + '</jsonCaseCollection>\
          </InsertE3HandoverData>\
        </soap:Body>\
      </soap:Envelope>';


        axios.post('https://testflight.lippohomes.com/InternalMobileAppsServiceDev/WS_TMD.asmx?op=InsertE3HandoverData',
            xmls,
            {
                headers:
                {
                    'Content-Type': 'text/xml'

                }
            }).then(res => {
                var toJson = [];
                response = new XMLParser().parseFromString(res.data);
                toJson = response.getElementsByTagName('InsertE3HandoverDataResult')

                if (toJson[0].value != "FAILED") {

                    this.setState({
                        bastID: toJson[0].value,

                    })


                }
                console.log('XML Body getInsertE3HandoverData ', xmls)
                console.log('Hasil getInsertE3HandoverData ', res.data)
                callback();

            }).catch(err => {
                console.log('SOAP ERROR getInsertE3HandoverData ', err)

            });
    }


    saveAttachmentRequest(i) {
        var response;
        var XMLParser = require('react-xml-parser');

        let xmls = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\
        <soap:Header>\
        <AuthHeader xmlns="http://tempuri.org/">\
        <domainName>KARAWACINET</domainName>\
        <userName>UsrE3Handover</userName>\
        <password>H4nd0v3rE3!</password>\
        </AuthHeader>\
        </soap:Header>\
        <soap:Body>\
          <SaveAttachment xmlns="http://tempuri.org/">\
            <caseNumber>'+ this.state.caseNumber + '</caseNumber>\
            <file>'+ this.state.detailDefect[i].imagebase64 + '</file>\
            <description>'+ this.state.detailDefect[i].description + '</description>\
            <type>'+ this.state.type + '</type>\
            <fileType>'+ this.state.fileType + '</fileType>\
          </SaveAttachment>\
        </soap:Body>\
      </soap:Envelope>';


        axios.post('https://testflight.lippohomes.com/InternalMobileAppsServiceDev/WS_TMD.asmx?op=SaveAttachment',
            xmls,
            {
                headers:
                {
                    'Content-Type': 'text/xml'

                }
            }).then(res => {

                response = new XMLParser().parseFromString(res.data);


                console.log('XML Save Attachment ', xmls);
                console.log('Hasil Save Attachment ', res.data);

            }).catch(err => { console.log('SOAP ERROR Save Attachment ', err) });
    }



    sendRequestCreateUniversal() {
        console.log('Start Create Universal ');
        if (this.state.detailDefect.length === 0) {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)


                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailBAST: this.state.imageBASTArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)

                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)
                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )

                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)
                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        bastid: this.state.bastID
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)
                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        } else {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailDPP: this.state.imagePinjamArray,
                        detail: this.state.detailDefect
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)
                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailBAST: this.state.imageBASTArray,
                        detail: this.state.detailDefect
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)

                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray,
                        detail: this.state.detailDefect
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)

                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detail: this.state.detailDefect
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)

                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.UNIVERSAL_CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        bastid: this.state.bastID,
                        detailBAST: this.state.imageBASTArray,
                        detail: this.state.detailDefect
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                console.log('Response Create ', response.data)

                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('CREATE UNIVERSAL ',
                                    'Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', BAST ID = ' + this.state.bastID +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + this.state.detailDefect
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        }
    }

    sendRequestCreateHO(detailNew) {
        this.setState({
            isLoadingButton: true,
            onCreateStatus: true,
            editableTextField: false
        })

        if (detailNew.length === 0) {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )



                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()

                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                            } else {
                                alert(response.data.result.message)

                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                Alert.alert(
                                    'Success',
                                    'Create Handover Successfully!',
                                    [
                                        { text: 'OK', onPress: () => this.props.navigation.navigate('ListHandoverScreen') },
                                    ],
                                    { cancelable: false },
                                );
                                this.setState({
                                    isLoadingButton: false
                                })
                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        } else {
            if (this.params.statusPaymentCode != 'LNS') {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailDPP: this.state.imagePinjamArray,
                        detail: detailNew
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )

                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else if (this.state.imagePinjamArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detail: detailNew
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        isDPP: this.state.isCheckedPinjam,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detailDPP: this.state.imagePinjamArray,
                        detail: detailNew
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            } else {
                if (this.state.imageBASTArray.length === 0) {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detail: detailNew
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                } else {
                    axios.post(UrlApi.CREATE_HANDOVER, {
                        invitationID: this.params.invitationID,
                        isTTDBAST: this.state.isCheckedBast,
                        bastDate: this.state.bastDate,
                        bastNo: this.state.bastNo,
                        isSerahterimaKunci: this.state.isCheckedSerah,
                        isSmartKey: this.state.isCheckedSmartKey,
                        isHandoverKit: this.state.isCheckedHandover,
                        isTatatertib: this.state.isCheckedTata,
                        isBonus: this.state.isCheckedBonus,
                        bonusDescription: this.state.bonusDescription,
                        detailBAST: this.state.imageBASTArray,
                        detail: detailNew
                    }, {
                        headers: {
                            'content-type': 'application/json',
                            'Authorization': 'Bearer ' + this.state.token
                        }
                    })
                        .then((response) => {
                            if (response.data.result.status) {
                                this.getParamApiLK()
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )


                            } else {
                                alert(response.data.result.message)
                                console.log('Invitation ID = ', this.params.invitationID +
                                    ', isTTBAST = ' + this.state.isCheckedBast +
                                    ', bastDate =' + this.state.BASTdate +
                                    ', bastNo = ' + this.state.bastNo +
                                    ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                    ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                    ', isHandoverKit = ' + this.state.isCheckedHandover +
                                    ', isTatatertib = ' + this.state.isCheckedTata +
                                    ', isBonus = ' + this.state.isCheckedBonus +
                                    ', isDPP = ' + this.state.isCheckedPinjam +
                                    ', bonusDescription = ' + this.state.bonusDescription +
                                    ', detailDPP = ' + this.state.imagePinjamArray +
                                    ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                    ', detail = ' + detailNew
                                )
                                this.setState({
                                    isLoadingButton: false
                                })
                            }
                        })
                        .catch((error) => {
                            console.log(error);
                            console.log('Invitation ID = ', this.params.invitationID +
                                ', isTTBAST = ' + this.state.isCheckedBast +
                                ', bastDate =' + this.state.BASTdate +
                                ', bastNo = ' + this.state.bastNo +
                                ', isSerahterimaKunci = ' + this.state.isCheckedSerah +
                                ', isSmartKey = ' + this.state.isCheckedSmartKey +
                                ', isHandoverKit = ' + this.state.isCheckedHandover +
                                ', isTatatertib = ' + this.state.isCheckedTata +
                                ', isBonus = ' + this.state.isCheckedBonus +
                                ', isDPP = ' + this.state.isCheckedPinjam +
                                ', bonusDescription = ' + this.state.bonusDescription +
                                ', detailDPP = ' + this.state.imagePinjamArray +
                                ', detailBAST = ' + this.state.imageBASTArray.toString() +
                                ', detail = ' + detailNew
                            )
                            Alert.alert('Warning', error.response.data.error.message,
                                [{
                                    text: 'OK',
                                    onPress: () => this.props.navigation.navigate('ListHandoverScreen')
                                }],
                                { cancelable: false }
                            );
                        });
                }
            }
        }
    }



    render() {
        return (
            <View style={{
                backgroundColor: '#EFEFEF', flex: 1,
                alignItems: "center"
            }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                        >Detail Customer</Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Customer Name
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.psName}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Project
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.project}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Unit Code
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.unitcode}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.containerItem}>
                            <View style={styles.item50}>
                                <Text>
                                    Unit No
                                </Text>
                            </View>
                            <View style={styles.item50}>
                                <Text>
                                    :  {this.params.unitno}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                        >Checklist</Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Tandatangan BAST
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    ref={(check) => { this.checkBox = check; }}
                                    value={this.state.isCheckedBast}
                                    disabled={this.state.onCreateStatus}
                                    onChange={() => this.checkBoxBastOnChage()}>
                                </CheckBox>
                            </View>
                            <TouchableHighlight style={styles.item5}
                                onPress={() =>
                                    this.state.isCheckedBast == true ?
                                        this.takePhotoBAST(false) : this.alertMessage('Tandatangan BAST')}
                                disabled={this.state.onCreateStatus}
                                activeOpacity={1}>
                                <Image
                                    source={require('../assets/camera.png')}

                                    style={{ height: 20, width: 20, resizeMode: 'contain', marginTop: 5 }}>
                                </Image>
                            </TouchableHighlight>
                            {this.state.isCheckedBast == true ? <View style={styles.item30}>
                                <TextInput
                                    onChangeText={(bastNo) => this.setState({ bastNo: bastNo })}
                                    value={this.state.bastNo}
                                    editable={this.state.editableTextField}
                                    maxLength={500}
                                    style={styles.textInput}></TextInput>
                            </View> : <View />}
                            {/* {this.state.imageBASTbase64 == '' ? <View/> : 
                                <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={{uri: imageBase64Bast}}/>
                            } */}
                        </View>
                        <View style={styles.containerItemTwo}>
                            {this.multipleImageBast()}
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Serah Terima Kunci
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    value={this.state.isCheckedSerah}
                                    disabled={this.state.onCreateStatus}
                                    onChange={() => this.checkBoxSerahOnChage()}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Serah Terima Smart Key
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    disabled={this.state.onCreateStatus}
                                    value={this.state.isCheckedSmartKey}
                                    onChange={() => this.checkBoxSmartKeyOnChage()}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Handover Kit
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    disabled={this.state.onCreateStatus}
                                    value={this.state.isCheckedHandover}
                                    onChange={() => this.checkBoxHandoverOnChage()}>
                                </CheckBox>
                            </View>
                        </View>
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <View style={styles.containerItemTwo}>
                                    <View>
                                        <Text>
                                            Tata Tertib Sarusun
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    disabled={this.state.onCreateStatus}
                                    value={this.state.isCheckedTata}
                                    onChange={() => this.checkBoxTataOnChage()}>
                                </CheckBox>
                            </View>
                        </View>
                        {this.params.statusPaymentCode != 'LNS' ?
                            <View>
                                <View style={styles.containerItemTwo}>
                                    <View style={styles.item50}>
                                        <View style={styles.containerItemTwo}>
                                            <View>
                                                <Text>
                                                    Dok. Pinjam Pakai
                                            </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.item5}>
                                        <CheckBox
                                            disabled={this.state.onCreateStatus}
                                            value={this.state.isCheckedPinjam}
                                            onChange={() => this.checkBoxPinjamOnChage()}>
                                        </CheckBox>
                                    </View>
                                    <TouchableHighlight style={styles.item5}
                                        onPress={() =>
                                            this.state.isCheckedPinjam == true ?
                                                this.takePhotoPinjam(false) : this.alertMessage('Dokument Pinjam Pakai')}
                                        disabled={this.state.onCreateStatus}
                                        activeOpacity={1}>
                                        <Image
                                            source={require('../assets/camera.png')}
                                            style={{ height: 20, width: 20, resizeMode: 'contain', marginTop: 5 }}>
                                        </Image>
                                    </TouchableHighlight>
                                    {/* {this.state.imagePinjambase64 == '' ? <View/> : 
                                    <Image style={{width: 50, height: 50, resizeMode: 'contain'}} source={{uri: imageBase64Pinjam}}/>
                                    } */}
                                </View>
                                <View style={styles.containerItemTwo}>
                                    {this.multipleImagePinjam()}
                                </View>
                            </View> : <View />}
                        <View style={styles.containerItemTwo}>
                            <View style={styles.item50}>
                                <Text>
                                    Bonus
                                </Text>
                            </View>
                            <View style={styles.item5}>
                                <CheckBox
                                    disabled={this.state.onCreateStatus}
                                    value={this.state.isCheckedBonus}
                                    onChange={() => this.checkBoxBonusOnChage()}>
                                </CheckBox>
                            </View>
                            {this.state.isCheckedBonus == true ?
                                <View style={styles.item40}>
                                    <TextInput
                                        onChangeText={(bonusDescription) => this.setState({ bonusDescription: bonusDescription })}
                                        value={this.state.bonusDescription}
                                        maxLength={500}
                                        style={styles.textInput}
                                        autoCapitalize='characters'
                                        editable={this.state.editableTextField}></TextInput>
                                </View> : <View />}

                        </View>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text
                            style={styles.title}
                        >Defect List</Text>
                    </View>
                    <View style={styles.detailCustomerContainer}>
                        {this.renderRoomDefect()}
                    </View>
                    {/* <View>
                    <FlatList
                    style={{marginBottom: 150}}
                        data={this.state.valueArray}
                        renderItem={({item}) => (
                            <Text>{item.image}</Text>
                        )}></FlatList>
                    </View> */}
                    <View style={{
                        margin: 10,
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        {this.state.isLoadingButton == false ?
                            <Button
                                title="SUBMIT"
                                color="#CB2406"
                                onPress={() =>
                                    this.createHandover()}
                            /> : <ActivityIndicator />}
                    </View>
                </ScrollView>
                <Modal
                    isOpen={this.state.isModalImageDefect}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 380,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImageDefect: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupDefect }} />
                </Modal>
                <Modal
                    isOpen={this.state.isModalImageBAST}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 405,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImageBAST: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupBAST }} />
                    <Text
                        style={{ textAlign: 'center', marginTop: 7, color: '#CB2406', textDecorationLine: 'underline' }}
                        onPress={() => this.removeBAST(this.state.removeImagePopupBASTindex)}
                    >Delete</Text>
                </Modal>
                <Modal
                    isOpen={this.state.isModalImagePinjam}
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowRadius: 10,
                        width: 340,
                        height: 405,
                        padding: 10
                    }}
                    position='center'
                    backdrop={true}
                    onClosed={() => this.setState({
                        isModalImagePinjam: false
                    })}>
                    <Image style={{ height: 370, resizeMode: 'contain' }} source={{ uri: this.state.imagePopupPinjam }} />
                    <Text
                        style={{ textAlign: 'center', marginTop: 7, color: '#CB2406', textDecorationLine: 'underline' }}
                        onPress={() => this.removePinjam(this.state.removeImagePopupPinjamindex)}
                    >Delete</Text>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    titleContainer: {
        borderBottomWidth: 1,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: 'white',
        marginTop: 10,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 5,
    },

    detailCustomerContainer: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 5,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginBottom: 5
    },

    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 8
    },

    containerItem: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item50: {
        width: '40%',
    },

    containerItemTwo: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    item20: {
        width: '10%',
        marginTop: -5,
    },

    item5: {
        width: '5%',
        marginTop: -5,
        marginRight: 15,
    },

    item30: {
        width: '30%',
    },

    item40: {
        width: '40%'
    },

    textInput: {
        borderWidth: 1,
        borderColor: '#cecece',
        height: 25,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        fontSize: 11,
        paddingVertical: 2,
        paddingHorizontal: 6
    },

    cardDefect: {
        margin: 5,
        padding: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderWidth: 1,
        borderColor: '#CB2406',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    marginTop: {
        marginTop: 5
    },

    noPhoto: {
        backgroundColor: '#DDD9D9',
        paddingHorizontal: 5,
        paddingVertical: 5,
        width: '7%',
        marginTop: 3,
        marginBottom: 3,
        marginRight: 3,
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    }
})