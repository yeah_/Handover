/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  StyleSheet, 
  TextInput,
  View, 
  Alert,
  AsyncStorage,
  BackHandler,
  TouchableHighlight,
  ActivityIndicator,
  Text, FlatList} from 'react-native';
import UrlApi from '../config/UrlApi';
import axios from 'axios';
import { Dropdown } from 'react-native-material-dropdown';
import Moment from 'moment';
import Modal from 'react-native-modalbox';

export default class ListHandover extends Component {
    static navigationOptions = {
       title: "List Hand Over",
       headerLeft: null,
    }
    constructor(props){
        super(props);
        this.today = new Date();
        this.m = this.today.getMonth('MM')+1;
        this.mS = this.m.toString();
        this.lengthmS = this.mS.length; 
        this.monthValue = '';
        if(this.lengthmS==1){
            this.monthValue = '0'+this.mS
        } else {
            this.monthValue = this.mS
        }
        this.state ={ 
            listInvitation: [],
            isLoading: true,
            isRefreshing: false,
            months: [],
            years: [],
            search:'',
            message: '',
            month: this.monthValue,
            year: this.today.getFullYear(),
            isModalImage: false,
            date:new Date(),
            status: [],
            invitationStatus:'-',
            periode: [],
            periodId: '-',
            confirmDateData: [],
            confirmDate:'-',
            unitCodeData: [],
            unitcode: '-',
            paymentStatusData: [],
            paymentStatus: 'Nothing Selected',
            isFind:false,
            token:''
        }
        this.navigate = this.props.navigation.navigate;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
      };

    handleRefresh = () =>{
        this.setState({
            isRefreshing: true,
        },
        ()=>{
            this.loadListHandover();
        })
    };

    componentWillMount(){
        this.getToken();
        this._subscribe = this.props.navigation.addListener('didFocus', () => {
            this.loadListHandover();
        });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount (){
        this.loadMonths();
        this.loadYears();
        this.loadStatus();
        this.loadPeriode();
        this.loadConfirmDate();
        this.loadUnitCode();
        this.loadPaymentStatus();
    };

    logout = async() =>{
        try {
          const token = await AsyncStorage.removeItem('accessToken');
          const username = await AsyncStorage.removeItem('username');
          const password = await AsyncStorage.removeItem('password');
          if (token == null && username == null && password == null) {
            this.navigate('LoginScreen')
          } else {
              alert(value)
          }
        } catch (error) {
          // Error retrieving data
        }
    }

    async getToken(){
        try{
            let token = await AsyncStorage.getItem('accessToken');
            return this.setState({
                token : token
            })
        }
        catch(e){
            console.log('caught error', e);
            // Handle exceptions
        }
    
    }

    handleBackButtonClick() {
        Alert.alert(
            'Exit App',
            'Are you sure you want to logout?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => this.logout()
            }, ], {
                cancelable: false
            }
         )
        return true;
    }

    loadMonths = ()=> {
        this.setState({
            months : [
                {
                    number: '01',
                    name: 'Jan'
                },
                {
                    number: '02',
                    name: 'Feb'
                },
                {
                    number: '03',
                    name: 'Mar'
                },
                {
                    number: '04',
                    name: 'Apr'
                },
                {
                    number: '05',
                    name: 'May'
                },
                {
                    number: '06',
                    name: 'Jun'
                },
                {
                    number: '07',
                    name: 'Jul'
                },
                {
                    number: '08',
                    name: 'Aug'
                },
                {
                    number: '09',
                    name: 'Sep'
                },
                {
                    number: '10',
                    name: 'Oct'
                },
                {
                    number: '11',
                    name: 'Nov'
                },
                {
                    number: '12',
                    name: 'Dec'
                }
            ]
        })
    }

    loadYears = () => {
        var tahun = [];
        year = this.today.getFullYear();
        for(var i=year; i>=2000; i--){
            tahun.push({
                title: i
            })
        }
        this.setState({
            years: tahun
        })
    }

    loadStatus = ()=>{
        axios.get(UrlApi.GET_STATUS_INVITATION, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          }) 
              .then((response) => {
                  if (response.data.result.status) {
                    const obj = {id: '-', name: 'All'}
                    this.setState({
                        status : response.data.result.data
                    })
                    this.setState({
                        status: [obj, ...this.state.status]
                    })
                }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadPeriode = ()=>{
        axios.get(UrlApi.GET_PERIODE_LIST, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
              .then((response) => {
                  if (response.data.result.status) {
                    const obj = {id: '-', name: 'All'}
                    this.setState({
                        periode : response.data.result.data
                    })
                    this.setState({
                        periode: [obj, ...this.state.periode]
                    })
                }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadConfirmDate = ()=>{
        var month = parseInt(this.state.month)
        var year = parseInt(this.state.year)
        axios.get(UrlApi.GET_CONFIRM_DATE_LIST+month+UrlApi.YEAR+year, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
              .then((response) => {
                  if (response.data.result.status) {
                    const obj = {id: '-', name: 'Nothing Selected'}
                    this.setState({
                        confirmDateData : response.data.result.data,
                    })
                    this.setState({
                        confirmDateData : [obj, ...this.state.confirmDateData]
                    })
                }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadUnitCode = ()=>{
        var month = parseInt(this.state.month)
        var year = parseInt(this.state.year)
        axios.get(UrlApi.GET_UNIT_CODE_LIST+month+UrlApi.YEAR+year, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
              .then((response) => {
                  if (response.data.result.status) {
                    const obj = {id: '-', name: 'Nothing Selected'}
                    this.setState({
                        unitCodeData : response.data.result.data,
                    })
                    this.setState({
                        unitCodeData : [obj, ...this.state.unitCodeData]
                    })
                      
                }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadPaymentStatus = () =>{
        axios.get(UrlApi.GET_PAYMENT_STATUS, {
            headers:{
              'content-type': 'application/json',
              'Authorization': 'Bearer '+this.state.token
            }
          })
              .then((response) => {
                  if (response.data.result.status) {
                    const obj = {id: '-', name: 'Nothing Selected'}
                    this.setState({
                        paymentStatusData : response.data.result.data,
                    })
                    this.setState({
                        paymentStatusData : [obj, ...this.state.paymentStatusData]
                    }) 
                }
              })
              .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    }

    loadListHandover = () =>{
        
        var month = parseInt(this.state.month);
        var year = parseInt(this.state.year);
        var url = '';
        var urlSearch = '';
        var urlInvitation = '';
        var urlPeriodID = '';
        var urlConfirmDate = '';
        var urlUnitcode = '';
        var urlPaymentStatus = '';
        this.setState({
            isLoading: true,
            isRefreshing: true,
            message: ''
          })
        if (this.state.search.length!= 0){
            urlSearch = UrlApi.TEXT_SEARCH+this.state.search
        } else {
            urlSearch = '';
        }
        if (this.state.invitationStatus != '-'){
            urlInvitation = UrlApi.INVITATION_STATUS+this.state.invitationStatus
        } 
        if (this.state.periodId != '-'){
            urlPeriodID = UrlApi.PERIODE_ID+this.state.periodId
        }
        if(this.state.confirmDate != '-'){
            urlConfirmDate = UrlApi.CONFIRM_DATE+this.state.confirmDate
        }
        if(this.state.unitcode != '-'){
            urlUnitcode = UrlApi.UNIT_CODE+this.state.unitcode
        }
        if(this.state.paymentStatus != 'Nothing Selected'){
            urlPaymentStatus = UrlApi.PAYMENT_STATUS+this.state.paymentStatus
        }
        
        url = UrlApi.GET_ALL_INVITATION+month+UrlApi.CONFIRM_DATE_YEAR+year+urlSearch+
                urlInvitation+urlPeriodID+urlConfirmDate+urlUnitcode+urlPaymentStatus
        
        axios.get(url, {
          headers:{
            'content-type': 'application/json',
            'Authorization': 'Bearer '+this.state.token
          }
        })
            .then((response) => {
                if (response.data.result.status) {
                    if(response.data.result.data.length == 0){
                        this.setState({
                            message: 'No List Detected!',
                            isRefreshing: false,
                            isLoading: false
                        })
                    } else {
                        this.setState({
                            listInvitation : response.data.result.data,
                            isRefreshing: false,
                            isLoading: false
                          })
                    }
                } else {
                  this.setState({
                    isLoading: false,
                    isRefreshing: false
                  })
                    alert(response.data.result.message)
                }
            }) 
            .catch((error)=> {
                console.log(error);
                Alert.alert('Warning',error.response.data.error.message,
                [{
                  text: 'OK',
                  onPress: () => this.navigate('LoginScreen')}],
                  {cancelable: false}
                );
            });
    };

    onChangeSearch (search) {
        this.setState({
            search:search
        })
                
        if(this.state.listInvitation.length!=0){
            this.setState({
                isLoading: true,
                isRefreshing: true,
                message: ''
              })
            var month = parseInt(this.state.month);
            var year = parseInt(this.state.year);
            var url = '';
            var urlSearch = '';
            var urlInvitation = '';
            var urlPeriodID = '';
            var urlConfirmDate = '';
            var urlUnitcode = '';
            if (search.length!= 0){
                urlSearch = UrlApi.TEXT_SEARCH+search
            } else {
                urlSearch = '';
            }

            if (this.state.invitationStatus != '-'){
                urlInvitation = UrlApi.INVITATION_STATUS+this.state.invitationStatus
            } 
            if (this.state.periodId != '-'){
                urlPeriodID = UrlApi.PERIODE_ID+this.state.periodId
            }
            if(this.state.confirmDate != '-'){
                urlConfirmDate = UrlApi.CONFIRM_DATE+this.state.confirmDate
            }
            if(this.state.unitcode != '-'){
                urlUnitcode = UrlApi.UNIT_CODE+this.state.unitcode
            }
            
            url = UrlApi.GET_ALL_INVITATION+month+UrlApi.CONFIRM_DATE_YEAR+year+urlSearch+
                    urlInvitation+urlPeriodID+urlConfirmDate+urlUnitcode
            
            console.log(url)

            axios.get(url, {
              headers:{
                'content-type': 'application/json',
                'Authorization': 'Bearer '+this.state.token
              }
            })
                .then((response) => {
                    if (response.data.result.status) {
                        if(response.data.result.data.length == 0){
                            this.setState({
                                message: 'No List Detected!',
                                isRefreshing: false,
                                isLoading: false
                            })
                        } else {
                            this.setState({
                                listInvitation : response.data.result.data,
                                isRefreshing: false,
                                isLoading: false
                              })
                        }
                    } else {
                      this.setState({
                        isLoading: false,
                        isRefreshing: false
                      })
                        alert(response.data.result.message)
                    }
                })
                .catch((error)=> {
                    console.log(error);
                    Alert.alert('Warning',error.response.data.error.message,
                    [{
                      text: 'OK',
                      onPress: () => this.navigate('LoginScreen')}],
                      {cancelable: false}
                    );
                });
            
        }
    }

    onChangeMonth = (value) => {
        this.setState({
            month: value
        })
        this.loadConfirmDate()
        this.loadUnitCode()
        this.loadListHandover()
    }

    onChangeYear = (value) => {
        this.setState({
            year: value
        })
        this.loadConfirmDate()
        this.loadUnitCode()
        this.loadListHandover()
    }

    onChangeStatus = (value) => {
        this.setState({
            invitationStatus: value
        })
    }

    onChangePeriode= (value) => {
        this.setState({
            periodId: value
        })
    }

    onChangeConfirmDate= (value) => {
        console.log('Confirm Date '+value)
        this.setState({
            confirmDate: value
        })
    }

    onChangeUnitcode = (value)=>{
        this.setState({
            unitcode: value
        })
    }

    onChangePaymentStatus = (value)=>{
        this.setState({
            paymentStatus: value
        })
    }

    showAdvanceFilter(){
        this.setState({
            isModalImage : true
        })
    }

    filter(){
        this.setState({
            isModalImage: false
        })
        this.loadListHandover();
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'stretch',
              }}>
                <TextInput style={style.textInput}
                    onChangeText={(search)=>this.onChangeSearch(search)}
                    value={this.state.search}
                    placeholder="Search"
                />
                <View style={{flexDirection: 'row',
                        flexWrap: 'wrap',
                        alignItems: 'flex-start',
                        marginHorizontal: 10,
                        justifyContent: 'center'}}>
                    <Dropdown
                        containerStyle={{width: '45%', marginHorizontal: 5}}
                        label='Month'
                        value={this.state.month}
                        data={this.state.months}
                        onChangeText={(number)=>this.onChangeMonth(number)}
                        valueExtractor={({number})=> number}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />
                    <Dropdown
                        containerStyle={{width: '45%', marginHorizontal: 5}}
                        label='Year'
                        value={this.state.year}
                        data={this.state.years}
                        onChangeText={(title)=>this.onChangeYear(title)}
                        valueExtractor={({title})=> title}
                        labelExtractor={({title})=> title} 
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />
                </View>
                <View style={[style.rightPosition, {margin: 10}]}>
                    <Text style={{color: '#1E54E4', textDecorationLine:'underline'}} onPress={()=>this.showAdvanceFilter()}>Advance Filter</Text>
                </View>
                {this.state.isLoading == true ?
                <ActivityIndicator/> : this.state.message!=''?
                <View  style={{margin: 10, alignItems: 'center', alignContent: 'center', justifyContent:'center'}}>
                <Text>{this.state.message}</Text>
                </View>
                : 
                <View>    
                    <FlatList
                    style={{marginBottom: 200}}
                        data={this.state.listInvitation}
                        renderItem={({item}) => (
                            <View style={style.cardView}>
                                <View style={style.item35}>
                                    <View style={{margin: 10}}>
                                    {item.confirmDate == null ?
                                        <View style={{alignItems:'center', alignContent:'center', justifyContent:'center'}}>
                                            <Text style={{fontSize: 12}}>No Confirm Date</Text>
                                            <Text style={{fontSize: 8, marginTop: 5}}>{item.statusPayment}</Text>
                                            <Text style={{fontSize: 7, marginTop: 5}}>{item.periodDescription}</Text>
                                        </View> 
                                    :   <View style={{alignItems:'center', alignContent:'center', justifyContent:'center'}}>
                                            <Text style={{fontSize: 16}}>{Moment(item.confirmDate).format('DD')}</Text>
                                            <Text style={{fontSize: 20}}>{Moment(item.confirmDate).format('MMM')}</Text>
                                            <Text style={{fontSize: 16}}>{Moment(item.confirmDate).format('YYYY')}</Text>
                                            <Text style={{fontSize: 8, marginTop: 5}}>{item.statusPayment}</Text>
                                            <Text style={{fontSize: 7, marginTop: 5}}>{item.periodDescription}</Text>
                                        </View>
                                    } 
                                    </View>
                                </View>
                                <View style={[style.leftLine, style.leftLine2]}/>
                                <View style={style.item60}>
                                    <Text style={{fontWeight: 'bold'}}>
                                        {item.psName}
                                    </Text>
                                    <Text style={{fontSize: 10}}>
                                        {item.unitcode}
                                    </Text>
                                    <Text style={{fontSize: 10}}>
                                        {item.unitno}
                                    </Text>
                                    <Text style={{fontSize: 10}}>
                                        {item.invitationStatusName}
                                    </Text>
                                        {item.invitationStatus == 2 || item.invitationStatus == 3 || item.invitationStatus == 4 || item.invitationStatus == 5 ? (
                                            <View style={style.containerItem}>
                                            <TouchableHighlight style={style.button}
                                            onPress={()=>this.navigate('ViewHandoverScreen', {
                                                invitationID: item.id,
                                                psName: item.psName,
                                                project:item.project,
                                                unitcode: item.unitcode,
                                                unitno: item.unitno,
                                                statusPaymentCode: item.statusPaymentCode
                                              })}>
                                                <Text>
                                                    View
                                                </Text>
                                            </TouchableHighlight> 
                                            <TouchableHighlight style={style.button}
                                            onPress={()=>this.navigate('EditHandoverScreen', {
                                                invitationID: item.id,
                                                psName: item.psName,
                                                project:item.project,
                                                unitcode: item.unitcode,
                                                unitno: item.unitno,
                                                statusPaymentCode: item.statusPaymentCode
                                              })}>
                                                <Text>
                                                    Edit
                                                </Text>
                                            </TouchableHighlight>     
                                            </View>) : (
                                            <TouchableHighlight style={style.button}
                                            onPress={()=>this.navigate('CreateHandoverScreen', {
                                                invitationID: item.id,
                                                psName: item.psName,
                                                project:item.project,
                                                unitcode: item.unitcode,
                                                unitno: item.unitno,
                                                statusPaymentCode: item.statusPaymentCode
                                              })}>
                                            <Text>
                                                Create
                                            </Text>
                                        </TouchableHighlight>
                                        )}
                                </View>
                            </View>
                        )}
                        keyExtractor={i => i.id.toString()}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.handleRefresh}
                    />
                </View>}
                <Modal
                isOpen={this.state.isModalImage}
                style={{
                    borderRadius:30,
                    shadowRadius: 10,
                    width: 340,
                    height: 450,
                    padding: 10
                }}
                position='center'
                backdrop={true}
                onClosed={()=>this.setState({
                    isModalImage: false
                })}>
                    <Text style={{fontWeight: 'bold', color: '#CB2406', margin: 10}}>Advance filter</Text>
                    <Dropdown
                        containerStyle={{marginHorizontal: 5,}}
                        label='Invitation Status'
                        data={this.state.status}
                        value={this.state.invitationStatus}
                        onChangeText={(id)=>this.onChangeStatus(id)}
                        valueExtractor={({id})=> id}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />
                    <Dropdown
                        containerStyle={{marginHorizontal: 5,}}
                        label='Periode'
                        data={this.state.periode}
                        value={this.state.periodId}
                        onChangeText={(id)=>this.onChangePeriode(id)}
                        valueExtractor={({id})=> id}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />
                    <Dropdown
                        containerStyle={{marginHorizontal: 5,}}
                        label='Confirm Date'
                        value={this.state.confirmDate}
                        data={this.state.confirmDateData}
                        onChangeText={(id)=>this.onChangeConfirmDate(id)}
                        valueExtractor={({id})=> id}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />    
                    <Dropdown
                        containerStyle={{marginHorizontal: 5,}}
                        label='Unit Code'
                        value={this.state.unitcode}
                        data={this.state.unitCodeData}
                        onChangeText={(id)=>this.onChangeUnitcode(id)}
                        valueExtractor={({id})=> id}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />    
                    <Dropdown
                        containerStyle={{marginHorizontal: 5,}}
                        label='Payment Status'
                        value={this.state.paymentStatus}
                        data={this.state.paymentStatusData}
                        onChangeText={(name)=>this.onChangePaymentStatus(name)}
                        valueExtractor={({name})=> name}
                        labelExtractor={({name})=> name}
                        itemColor='#000'
                        selectedItemColor='#BB0A0A'
                    />    
                    <View style={style.rightPosition}>
                        <TouchableHighlight style={style.buttonFilter}
                            onPress={()=>this.filter()}>
                            <Text style={{color: '#fff'}}>
                                FILTER
                            </Text>
                        </TouchableHighlight>
                    </View>    
            </Modal>
            </View>
        )
    }
}

const style = StyleSheet.create({
    scene: {
        flex: 1,
        paddingTop: 25,
    },
    textInput:{
        borderWidth: 1,
        borderColor: '#cecece',
        paddingHorizontal: 10,
        marginTop: 20,
        borderBottomLeftRadius: 7,
        borderBottomRightRadius: 7,
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
        marginHorizontal: 10
    },
    user: {
        width: '100%',
        backgroundColor: '#333',
        marginBottom: 10,
        paddingLeft: 25,
    },
    userName: {
        fontSize: 17,
        paddingVertical: 20,
        color: '#fff'
    },

    cardView: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: 'white',
        marginTop: 5,
        flex: 1,
        marginBottom: 5,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.8,
        shadowRadius: 10,  
        elevation: 5,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    leftLine : {
        right: 0,
        width: '2%',
        height: 115,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginHorizontal: 10,
        marginVertical: 7
    },

    leftLine0 : {
        backgroundColor: '#BB0A0A',
    },
    leftLine1 : {
        backgroundColor: '#FFC300',
    },
    leftLine2 : {
        backgroundColor: '#25E40A',
    },

    item60:{
        width: '65%',
        marginVertical: 7
    },

    item35: {
        width: '25%'
    },

    containerItem :{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },

    containerItemDD :{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        marginHorizontal: 10,
        width: '100%',
    },

    button :{
        backgroundColor: '#DDD9D9',
        width: '30%',
        marginRight: 15,
        marginTop: 10,
        paddingHorizontal: 5,
        paddingVertical: 5,
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },

    buttonFilter :{
        backgroundColor: '#CB2406',
        width: '30%',
        marginRight: 15,
        marginTop: 10,
        paddingHorizontal: 5,
        paddingVertical: 5,
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },

    rightPosition:{
        alignItems: 'flex-end', 
        alignContent: 'flex-end', 
        justifyContent:'flex-end'
    }

});


