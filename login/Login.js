/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  StyleSheet, 
  TextInput, 
  View, 
  Text,
  BackHandler,
  Button,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  Alert,
  AsyncStorage,
  Image} from 'react-native';
import UrlApi from '../config/UrlApi';
import axios from 'axios';

export default class Login extends Component {
    static navigationOptions = {
        header : null
    }

    constructor(){
      super();
      this.state ={ 
        isLoading: false,
        username: '',
        password: '',
      }
    }

    componentWillMount(){
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      BackHandler.exitApp();
      return true;
    }

    storeData = async(accessToken, username, password)=>{
      try {
        await AsyncStorage.setItem('accessToken', accessToken.toString());
        await AsyncStorage.setItem('username', username.toString());
        await AsyncStorage.setItem('password', password.toString());
      } catch (error) {
        alert(error)
      }
    }

    signIn(){
      if(this.state.username == '' && this.state.password == '' ) {
          Alert.alert('Warning', 'Please fill the blank field!')
      } else if(this.state.username == ''){
          Alert.alert('Warning', 'Please fill the username field!')
      } else if(this.state.password == ''){
          Alert.alert('Warning', 'Please fill the password field!')
      } else {
        this.setState({
          isLoading: true
        })
          axios.post(UrlApi.LOGIN, {
            userNameOrEmailAddress: this.state.username,
            password: this.state.password
        }, {
          headers:{
            'content-type': 'application/json'
          }
        })
            .then((response) => {
                if (response.data.success) {
                  this.storeData(response.data.result.accessToken, this.state.username, this.state.password)
                  this.setState({
                    isLoading: false
                  })
                  this.props.navigation.navigate('ListHandoverScreen')
                } else {
                  this.setState({
                    isLoading: false
                  })
                    alert(response.data.error.details)
                }
            }) 
            .catch((error)=> {
              console.log(error);
              Alert.alert('Warning',error.response.data.error.message,
              [{
                text: 'OK',
                onPress: () => this.setState({isLoading: false})}],
                {cancelable: false}
              );
          });
      }
    }

  render() {

    return (
    <View>
        <ImageBackground
        style={{width: '100%', height: '100%', alignContent: 'center'}}
        source={require('../assets/background-handover.jpeg')}>
        <KeyboardAvoidingView style={{flex: 1, padding: 25}} behavior="position" enabled keyboardVerticalOffset={-100}>
            <View style={{marginTop: 50, alignItems: 'center'}}>
              <Image source = {require('../assets/logo-meikarta-png-1.png')} />
            </View>
            <View style={{alignItems:'center'}}>
              <View style={{marginTop: 70}}> 
                <Text>
                  Username
                </Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Enter username"
                  returnKeyType='next'
                  onChangeText={(username)=> this.setState({username: username})}
                  value={this.state.username}
                  maxLength={50}
                  onSubmitEditing={()=> this.refs.txtPassword.focus()}>
                </TextInput>
              </View>
              <View style={{marginTop: 20}}> 
                <Text>
                  Password
                </Text>
                <TextInput
                  onChangeText={(password)=> this.setState({password: password})}
                  value={this.state.password}
                  maxLength={50}
                  style={styles.textInput}
                  placeholder="Enter password"
                  returnKeyType='go'
                  secureTextEntry={true}
                  ref={"txtPassword"}>
                </TextInput>
              </View>
            </View>
            <View style={{marginTop: 20, alignSelf: 'stretch'}}> 
                {this.state.isLoading == false ?
              <Button
              title = "SIGN IN"
              color = "#CB2406"
              onPress={() =>this.signIn()}
              />:<ActivityIndicator/> }
            </View>
        </KeyboardAvoidingView>
        </ImageBackground>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    position: 'absolute',
    opacity:0.2,
    height: '100%',
    width: '100%',
  },
  container: {
    flex:1,
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  logo: {
    width: 224,
    height: 140,
    top: 250
  },
  backdrop: {
    resizeMode: 'cover', 
    flex: 1
  },
  headline: {
    fontSize: 18,
    textAlign: 'center',
    backgroundColor: 'black',
    color: 'white'
  },

  text:{
    fontSize: 18,
    color: 'black',
  },
  textInput:{
    marginTop: 5,
    width: 300,
    fontSize:12,
    borderColor:'#FFFFFF',
    backgroundColor:'white',
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    paddingHorizontal : 10
  },
});
