import React, {Component} from 'react';
import {
  StyleSheet,
  AsyncStorage,
  Text, 
  Alert,
  View,
  Image} from 'react-native';
  import axios from 'axios';
  import UrlApi from '../config/UrlApi';

  export default class SplashScreen extends Component {
    static navigationOptions = {
        header : null
    }

    componentDidMount(){
      this.redirectScreen()
    } 

  removeToken = async() =>{
      try {
        await AsyncStorage.removeItem('accessToken');
      } catch (error) {
        // Error retrieving data
      }
  }

  storeData = async(accessToken, username, password)=>{
    try {
      await AsyncStorage.setItem('accessToken', accessToken.toString());
      await AsyncStorage.setItem('username', username.toString());
      await AsyncStorage.setItem('password', password.toString());
    } catch (error) {
      alert(error)
    }
  }

  checkLogin= async() =>{
    try {
      const username = await AsyncStorage.getItem('username');
      const password = await AsyncStorage.getItem('password');
      axios.post(UrlApi.LOGIN, {
            userNameOrEmailAddress: username,
            password: password
        }, {
          headers:{
            'content-type': 'application/json'
          }
        }).then((response) => {
              if (response.data.success) {
                this.removeToken()
                this.storeData(response.data.result.accessToken, username, password)
                this.setState({
                  isLoading: false
                })
                this.props.navigation.navigate('ListHandoverScreen')
              } else {
                  alert(response.data.error.details)
              }
          })
          .catch((error)=> {
            console.log(error);
            Alert.alert('Warning',error.response.data.error.message,
            [{
              text: 'OK',
              onPress: () => this.navigate('LoginScreen')}],
              {cancelable: false}
            );
        });

    } catch {
      
    }
  }

  redirectScreen = async() =>{
    try {
      const token = await AsyncStorage.getItem('accessToken');
      
      if (token !== null) {
        setTimeout(()=>{
          // this.props.navigation.navigate('ListHandoverScreen')
          this.checkLogin()
        }, 2000)
      } else {
        setTimeout(()=>{
          this.props.navigation.navigate('LoginScreen')
        }, 2000)
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  render(){
      return(
        <Image style={styles.image}
            source = {require('../assets/background-splash.jpg')} />
      );
  }
}

  const styles = StyleSheet.create({
      image:{
        resizeMode: 'cover', 
        width: '100%', 
        height:'100%', 
        justifyContent:'center'
      }
  })