import React, { Component } from 'react';
import { Root } from "native-base";
import { createAppContainer } from 'react-navigation';
import ModalRoute from './config/Router';

const App = createAppContainer(ModalRoute);
//export default App;


export default () =>
  <Root>
    <App />
  </Root>;